#include "TriviaServer.h"


static const unsigned short PORT = 8820;
static const unsigned int IFACE = 0;
TriviaServer::TriviaServer()
{
	this->_roomSequence = 0;
	// notice that we step out to the global namespace
	// for the resolution of the function socket
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}


TriviaServer::~TriviaServer()
{
	TRACE(__FUNCTION__ " closing accepting socket");
	// why is this try necessarily ?
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);
	}
	catch (...) {}
}
void TriviaServer::serve()
{
	bindAndListen();

	// create new thread for handling message
	std::thread tr(&TriviaServer::handleRecievedMessages, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TRACE("accepting client...");
		acceptClient();
	}
}
void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	TRACE("listening...");
}
void TriviaServer::acceptClient()
{
	SOCKET client_socket = accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	TRACE("Client accepted !");
	// create new thread for client	and detach from it
	std::thread tr(&TriviaServer::clientHandler, this, client_socket);
	tr.detach();
}
void TriviaServer::clientHandler(SOCKET client_socket)
{
	RecievedMessage* currRcvMsg = nullptr;
	try
	{
		while (true)
		{
			// get the first message code
			int msgCode = Helper::getMessageTypeCode(client_socket);
			currRcvMsg = buildRecieveMessage(client_socket, msgCode);
			addRecievedMessage(currRcvMsg);
		}
	}
	catch (const std::exception& e)
	{
		//std::cout << "Exception was catch in function clientHandler. socket=" << client_socket << ", what=" << e.what() << std::endl;
		currRcvMsg = buildRecieveMessage(client_socket, TR_CLIENT_SIGN_OUT);
		addRecievedMessage(currRcvMsg);
	}
	cout << "Exception was catch, closed client socket..." << endl;
	closesocket(client_socket);
}
void TriviaServer::handleRecievedMessages()
{
	int msgCode = 0;
	SOCKET clientSock = 0;
	string userName;
	RecievedMessage* currMessage = 0;
	while (true)
	{
		try
		{
			unique_lock<mutex> lck(_mtxRecievedMessages);

			// Wait for clients to enter the queue.
			if (_queRcvMessages.empty())
				_msgQueueCondition.wait(lck);

			// in case the queue is empty.
			if (_queRcvMessages.empty())
				continue;

			RecievedMessage* currMessage = _queRcvMessages.front();
			_queRcvMessages.pop();
			lck.unlock();
			// Extract the data from the tuple.
			clientSock = currMessage->getSock();
			msgCode = currMessage->getMessageCode();
			cout << "msgCode: "<< msgCode << endl;
			if (msgCode == TR_CLIENT_SIGN_IN)
			{
				cout << "Sign in request" << endl;
				handleSignIn(currMessage);
			}
			else if (msgCode == TR_CLIENT_SIGN_UP)
			{
				cout << "Sign up request" << endl;
				handleSignUp(currMessage);
			}
			else if (msgCode == TR_CLIENT_SIGN_OUT)
			{
				cout << "Sign out request" << endl;
				safeDeleteUser(currMessage);
			}
			else if (msgCode == TR_CLIENT_CREATE_ROOM)
			{
				cout << "Create room request" << endl;
				handleCreateRoom(currMessage);
			}
			else if (msgCode == TR_CLIENT_CLOSE_ROOM)
			{
				cout << "Close room request" << endl;
				handleCloseRoom(currMessage);
			}
			else if (msgCode == TR_CLIENT_JOIN_ROOM)
			{
				cout << "Join room request" << endl;
				handleJoinRoom(currMessage);
			}
			else if (msgCode == TR_CLIENT_GET_ROOM_LIST)
			{
				cout << "Get room list request" << endl;
				handleGetRooms(currMessage);
			}
			else if (msgCode == TR_CLIENT_GET_USERS_LIST)
			{
				cout << "Get users list request" << endl;
				handleGetUsersInRoom(currMessage);
			}
			else if (msgCode == TR_CLIENT_LEAVE_ROOM)
			{
				cout << "Leave room request" << endl;
				handleLeaveRoom(currMessage);
			}
			else if (msgCode == TR_CLIENT_START_GAME)
			{
				cout << "Start game request" << endl;
				handleStartGame(currMessage);
			}
			else if (msgCode == TR_CLIENT_LEAVE_GAME)
			{
				cout << "Leave game request" << endl;
				handleLeaveGame(currMessage);
			}
			else if (msgCode == TR_CLIENT_ANSWER_QUESTION)
			{
				cout << "Answer question request" << endl;
				handlePlayerAnswer(currMessage);
			}
			else if (msgCode == TR_CLIENT_GET_BEST_SCORES)
			{
				cout << "Get best scores request" << endl;
				handleGetBestScores(currMessage);
			}
			else if (msgCode == TR_CLIENT_GET_PERSONAL_STATUS)
			{
				cout << "Get personal status request" << endl;
				handleGetPersonalStatus(currMessage);
			}
			else
			{
				cout << "Unknown message code" << endl;
				safeDeleteUser(currMessage);
			}
			delete currMessage;
		}
		catch (...)
		{
			cout << "An error has occured" << endl;
			safeDeleteUser(currMessage);
		}
	}
}
RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET client_socket, int msgCode)
{
	RecievedMessage* msg = nullptr;
	vector<string> values;
	if (msgCode == TR_CLIENT_SIGN_IN)
	{
		int username_Length = Helper::getIntPartFromSocket(client_socket, 2);
		string username = Helper::getStringPartFromSocket(client_socket, username_Length);
		int password_Length = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, password_Length);
		cout << "Username: " << username << endl;
		cout << "Password: " << password << endl;
		values.push_back(username);
		values.push_back(password);
	}
	else if (msgCode == TR_CLIENT_SIGN_UP)
	{
		int username_Length = Helper::getIntPartFromSocket(client_socket, 2);
		string username = Helper::getStringPartFromSocket(client_socket, username_Length);
		int password_Length = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, password_Length);
		int email_length = Helper::getIntPartFromSocket(client_socket, 2);
		string email = Helper::getStringPartFromSocket(client_socket, email_length);
		cout << "Username: " << username << endl;
		cout << "Password: " << password << endl;
		cout << "Email :" << email << endl;
		values.push_back(username);
		values.push_back(password);
		values.push_back(email);
	}
	else if (msgCode == TR_CLIENT_CREATE_ROOM)
	{
		int roomName_Length = Helper::getIntPartFromSocket(client_socket, 2);
		string roomName = Helper::getStringPartFromSocket(client_socket, roomName_Length);
		string playerNumber = Helper::getStringPartFromSocket(client_socket, 1);
		string questionNumber = to_string(Helper::getIntPartFromSocket(client_socket, 2));
		string questionNumberInSec = to_string(Helper::getIntPartFromSocket(client_socket, 2));
		values.push_back(roomName);
		values.push_back(playerNumber);
		values.push_back(questionNumber);
		values.push_back(questionNumberInSec);
		cout << "room name: " << roomName << endl;
		cout << "player number: " << playerNumber << endl;
		cout << "question number: " << questionNumber << endl;
		cout << "question number in sec: " << questionNumberInSec << endl;
	}
	else if (msgCode == TR_CLIENT_JOIN_ROOM)
	{
		string roomId = Helper::getStringPartFromSocket(client_socket, 4);
		values.push_back(roomId);
		cout << "room id: " << roomId << endl;
	}
	else if (msgCode == TR_CLIENT_GET_USERS_LIST)
	{
		string roomId = Helper::getStringPartFromSocket(client_socket, 4);
		values.push_back(roomId);
		cout << "room id: " << roomId << endl;
	}
	else if (msgCode == TR_CLIENT_ANSWER_QUESTION)
	{
		string answerNumber = Helper::getStringPartFromSocket(client_socket, 1);
		string time = Helper::getStringPartFromSocket(client_socket, 2);
		values.push_back(answerNumber);
		values.push_back(time);
		cout << "Answer number: " << answerNumber << endl;
		cout << "Time: " << time << endl;
	}
	msg = new RecievedMessage(client_socket, msgCode, values);
	map<SOCKET, User*>::iterator it;
	it = this->_connectedUsers.find(msg->getSock());
	//message includes a connected user...
	if (it != this->_connectedUsers.end())
	{
		msg->setUser(this->_connectedUsers[msg->getSock()]);
	}
	else
	{
		msg->setUser(nullptr);
	}
	return msg;
}
void TriviaServer::addRecievedMessage(RecievedMessage* msg)
{
	unique_lock<mutex> lck(_mtxRecievedMessages);
	_queRcvMessages.push(msg);
	lck.unlock();
	_msgQueueCondition.notify_all();
}
User* TriviaServer::getUserByName(string name)
{
	for (std::map<SOCKET, User*>::iterator it = _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
	{
		if (name == it->second->getUserName())
		{
			return it->second;
		}
	}
	return nullptr;
}
User* TriviaServer::getUserBySocket(SOCKET client_socket)
{
	for (std::map<SOCKET, User*>::iterator it = _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
	{
		if (client_socket == it->second->getSocket())
		{
			return it->second;
		}
	}
	return nullptr;
}

Room* TriviaServer::getRoomByid(int id)
{
	for (std::map<int, Room*>::iterator it = _roomsList.begin(); it != _roomsList.end(); ++it)
	{
		if (id == it->second->getId())
		{
			return it->second;
		}
	}
	return nullptr;
}
User* TriviaServer::handleSignIn(RecievedMessage* msg)
{
	cout << "Handling sign in..." << endl;
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];
	cout << username << endl;
	cout << password << endl;
	SOCKET client_socket = msg->getSock();
	if (!this->_db.isUserAndPassMatch(username, password))
	{
		cout << "Failed logging in,client did never register" << endl;
		Helper::sendData(client_socket, "1021");
		return nullptr;
	}
	if (getUserByName(username))
	{
		cout << "Failed logging in,client is already conntected" << endl;
		Helper::sendData(client_socket, "1022");
		return nullptr;
	}
	User* user = new User(client_socket,username);
	this->_connectedUsers[client_socket] = user;
	//_connectedUsers.insert(pair<SOCKET, User*>(client_socket, user));
	cout << "Signed in successfully" << endl;
	Helper::sendData(client_socket, "1020");
	return user;
}
bool TriviaServer::handleSignUp(RecievedMessage* msg)
{
	cout << "Handling sign up" << endl;
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];
	string email = msg->getValues()[2];
	cout << username << endl;
	cout << password << endl;
	cout << email << endl;
	SOCKET client_socket = msg->getSock();
	if (!Validator::isPasswordValid(password))
	{
		cout << "Password doesn't meet the requirments" << endl;
		Helper::sendData(client_socket, "1041");
		return false;
	}
	if (!Validator::isUsernameValid(username))
	{
		cout << "Password doesn't meet the requirments" << endl;
		Helper::sendData(client_socket, "1043");
	}
	if (_db.isUserExists(username))
	{
		cout << "Failed sign up,user is already exist" << endl;
		Helper::sendData(client_socket, "1042");
		return false;
	}
	if (_db.addNewUser(username, password, email))
	{
		cout << "Signed up successfully" << endl;
		Helper::sendData(client_socket, "1040");
		return true;
	}
	else
	{
		cout << "Failed to sign up..." << endl;
		Helper::sendData(client_socket, "1044");
		return false;
	}
}
void TriviaServer::handleSignOut(RecievedMessage* msg)
{
	map<SOCKET, User*>::iterator it;
	if (msg->getUser() != nullptr)
	{
		handleCloseRoom(msg);
		handleLeaveRoom(msg);
		handleLeaveGame(msg);
		//there is a user belongs to this message
		it = _connectedUsers.find(msg->getSock());
		_connectedUsers.erase(it);
		cout << "Sign out successfully" << endl;
	}
	else
	{
		cout << "No connected user belongs to this message..." << endl;
	}
}
void TriviaServer::safeDeleteUser(RecievedMessage* msg)
{
	try
	{ 
		SOCKET client_socket = msg->getSock();
		handleSignOut(msg);
	}
	catch (...)
	{
		cout << "Coudln't delete user..." << endl;
	}
}
bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	cout << "Handling create room" << endl;
	if (msg->getUser() != nullptr)
	{
		string roomName = msg->getValues()[0];
		string playerNumber = msg->getValues()[1];
		string questionNumber = msg->getValues()[2];
		string questionTimeInSec = msg->getValues()[3];
		this->_roomSequence++;
		if (this->_connectedUsers[msg->getSock()]->createRoom(_roomSequence, roomName, atoi(playerNumber.c_str()), atoi(questionNumber.c_str()), atoi(questionTimeInSec.c_str())))
		{
			Room* r = this->_connectedUsers[msg->getSock()]->getRoom();
			this->_roomsList[_roomSequence] = r;
			cout << "Created room successfully" << endl;
			return true;
		}
	}
	cout << "Failed to create room" << endl;
	return false;
}
bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	cout << "Handling close room" << endl;
	if (msg->getUser() != nullptr)
	{
		int roomId = this->_connectedUsers[msg->getSock()]->closeRoom();
		if (roomId!=-1)
		{
			cout << roomId << endl;
			map<int, Room*>::iterator it;
			it = _roomsList.find(roomId);
			_roomsList.erase(it);
			cout << "Closed room successfully..." << endl;
			return true;
		}
	}
	cout << "Failed to close room..." << endl;
	return false;
}
bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	cout << "Handling join room" << endl;
	if (msg->getUser() != nullptr)
	{
		string roomId = msg->getValues()[0];
		Room* r = this->getRoomByid(atoi(roomId.c_str()));
		//room exists
		if (r != nullptr)
		{
			if (this->_connectedUsers[msg->getSock()]->joinRoom(r))
			{
				cout << "Joined room successfully" << endl;
				cout << this->_connectedUsers[msg->getSock()]->getRoom()->getUsersListMessage() << endl;
				return true;
			}
			//room is full
			cout << "Failed to join room,room is full" << endl;
			return false;
		}
	}
	//room not exist or other reason
	Helper::sendData(msg->getSock(), "1102");
	cout << "Failed to join room" << endl;
	return false;
}
bool TriviaServer::handleLeaveRoom(RecievedMessage * msg)
{
	if (msg->getUser() != nullptr)
	{
		this->_connectedUsers[msg->getSock()]->leaveRoom();
		cout << "Left room successfully" << endl;
		//Helper::sendData(msg->getSock(), "1120");
		return true;
	}
	cout << "Failed to leave room" << endl;
	return false;
}
void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	std::stringstream ss;
	cout << "Handling get rooms" << endl;
	string answer = "106";
	string roomsLength = to_string(size(this->_roomsList));
	ss << std::setw(4) << std::setfill('0') << roomsLength;
	answer += ss.str();
	for (map<int, Room*>::iterator it = _roomsList.begin(); it != _roomsList.end(); ++it)
	{
		ss.str("");
		ss.clear();
		ss << std::setw(4) << std::setfill('0') << to_string(it->first);
		answer += ss.str();
		ss.str("");
		ss.clear();
		ss << std::setw(2) << std::setfill('0') << to_string(size(it->second->getName()));
		answer += ss.str();
		ss.str("");
		ss.clear();
		answer += it->second->getName();
	}
	Helper::sendData(msg->getSock(), answer);
	cout << "Succeed" << endl;
}
void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	cout << "Handling get users in room" << endl;
	string roomId = msg->getValues()[0];
	string answer = "";
	std::stringstream ss;
	Room* r = this->_roomsList[atoi(roomId.c_str())];
	if (r != nullptr)
	{
		answer = r->getUsersListMessage();
	}
	else
	{
		answer = "1080";
	}
	cout << answer << endl;
	Helper::sendData(msg->getSock(), answer);
	cout << "Succeed" << endl;
}
void TriviaServer::handleStartGame(RecievedMessage* msg)
{
	cout << "Handling start game" << endl;
	User* u = msg->getUser();
	Room* r = u->getRoom();
	try
	{
		Game* g = new Game(r->getId(), this->_db, r->getQuestionNo(), r->getUsers());
		int roomId = r->getId();
		map<int, Room*>::iterator it;
		it = _roomsList.find(roomId);
		_roomsList.erase(it);
		vector<User*> v = r->getUsers();
		for (int i = 0; i < size(v); i++)
		{
			v[i]->setRoom(nullptr);
		}
		g->sendFirstQuestion();
		cout << "Started game successfully" << endl;
	}
	catch (...)
	{
		Helper::sendData(msg->getSock(), "1180");
		cout << "Failed to start game" << endl;
	}
}
void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{
	cout << "Handling leave game..." << endl;
	if (this->_connectedUsers[msg->getSock()]->leaveGame())
	{
		cout << "Left room successfully..." << endl;
	}
	else
	{
		cout << "Failed to leave room..." << endl;
	}
}
void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{
	cout << "Handling player answer..." << endl;
	User* u = msg->getUser();
	Game* g = u->getGame();
	if (g != nullptr)
	{
		cout << "Game is open" << endl;
		int answerNumber = atoi(msg->getValues()[0].c_str());
		int time = atoi(msg->getValues()[0].c_str());
		cout << answerNumber << endl;
		cout << time << endl;
		g->handleAnswerFromUser(u, answerNumber, time);
	}
}
void TriviaServer::handleGetBestScores(RecievedMessage* msg)
{
	std::stringstream ss;
	string message = "124";
	string username;
	string heighestScore;
	cout << "Handling best scores" << endl;
	vector<string> bestScores = this->_db.getBestScores();
	for (int i = 0; i < size(bestScores); i++)
	{
		username = split(bestScores[i], ',')[0];
		cout << username << endl;
		heighestScore = split(bestScores[i], ',')[1];
		cout << heighestScore << endl;
		ss << std::setw(2) << std::setfill('0') << to_string(username.length());
		message += ss.str();
		ss.str("");
		ss.clear();
		message += username;
		ss << std::setw(6) << std::setfill('0') << heighestScore;
		message += ss.str();
		ss.str("");
		ss.clear();
	}
	if (size(bestScores) == 0)
	{
		message += "00""00000000""00000000""000000";
	}
	else if (size(bestScores) == 1)
	{
		message += "00""00000000""000000";
	}
	else if (size(bestScores) == 2)
	{
		message += "00""000000";
	}
	cout << message << endl;
	Helper::sendData(msg->getSock(), message);
	cout << "Done" << endl;
}
void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg)
{
	cout << "Handling status" << endl;
	string message = "126";
	std::stringstream ss;
	vector<string> ps = this->_db.getPersonalStatus(msg->getUser()->getUserName());
	string gameCount;
	string correctAnswers;
	string wrongAnswers;
	string avgTime;
	cout << ps.size() << endl;
	for (int i = 0; i < size(ps); i++)
	{
		gameCount = split(ps[i], ',')[0];
		correctAnswers = split(ps[i], ',')[1];
		wrongAnswers = split(ps[i], ',')[2];
		avgTime = split(ps[i], ',')[3];
		cout << gameCount << endl;
		cout << correctAnswers << endl;
		cout << wrongAnswers << endl;
		cout << avgTime << endl;
		ss << std::setw(4) << std::setfill('0') << gameCount;
		message += ss.str();
		ss.str("");
		ss.clear();
		ss << std::setw(6) << std::setfill('0') << correctAnswers;
		message += ss.str();
		ss.str("");
		ss.clear();
		ss << std::setw(6) << std::setfill('0') << wrongAnswers;
		message += ss.str();
		ss.str("");
		ss.clear();
		ss << std::setw(2) << std::setfill('0') << split(avgTime,'.')[0];
		message += ss.str();
		ss.str("");
		ss.clear();
		ss << std::setw(2) << std::setfill('0') << split(avgTime, '.')[1];
		message += ss.str();
		ss.str("");
		ss.clear();
	}
	cout << message << endl;
	Helper::sendData(msg->getSock(), message);
	cout << "Done" << endl;
}
std::vector<std::string> TriviaServer::split(std::string strToSplit, char delimeter)
{
	std::stringstream ss(strToSplit);
	std::string item;
	std::vector<std::string> splittedStrings;
	while (std::getline(ss, item, delimeter))
	{
		splittedStrings.push_back(item);
	}
	return splittedStrings;
}