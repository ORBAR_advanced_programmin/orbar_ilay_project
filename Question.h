#pragma once
#include <string>

using namespace std;
class Question
{
public:
	Question(int id,string question,string correntAnswer,string answer2,string answer3,string answer4);
	~Question();
	int getId();
	string* getAnswers();
	string getQuestion();
	int getCorrectAnswerIndex();

private:
	int _id;
	string _question;
	string _answers[4];
	int _correctAnswerIndex;
	
};

