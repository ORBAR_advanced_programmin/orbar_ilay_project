#pragma once
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <unordered_map>
#include "sqlite3.h"
class User;
class Room;
class Question;
using namespace std;

class DataBase
{
public:

	DataBase();
	~DataBase();
	void clearTable();
	string ExePath();
	static int callback(void * notUsed, int argc, char** argv, char** azCol);
	bool isUserExists(string username);
	bool addNewUser(string username, string password,string email);
	bool isUserAndPassMatch(string username, string password);
	vector<Question*> initQuestions(int questionNum);
	bool UpdateGameStatus(int id);
	int insertNewGame();
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string name);
	bool addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime);
	map<string, vector<string>> _users;
	static int callbackCount(void *, int, char**, char**);
	//static int callbackQuestions(void *, int, char**, char**);
	static int callbackBestScores(void *, int, char**, char**);
	static int callbackPersonalStatus(void *notUsed, int argc, char** argv, char**azCol);
	string quotesql(const string& s);
	int printTable();
private:
	sqlite3 *_db;
	User * _tmpUser;
	int _rc;
};

