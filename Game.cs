﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
namespace TriviaClient
{
    public partial class Game : Form
    {
        public Socket soc;
        public int questionTimeG;
        public int time = 0;
        public static int score = 0;
        public static int questionId = 1;
        public bool closeClicked = false;
        public Game(Socket socket,string roomN,string question,string answer1,string answer2,string answer3,string answer4,string questionsNum,string questionTime)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            exitButton.DialogResult = DialogResult.Abort;
            soc = socket;
            roomNameLabel.Text = roomN;
            questionLabel.Text = question;
            questionNumberLabel.Text = "Question " + questionId.ToString() + "\\" + questionsNum;
            answer1button.Text = answer1;
            answer2Button.Text = answer2;
            answer3Button.Text = answer3;
            answer4Button.Text = answer4;
            scoreValueLabel.Text = score.ToString();
            questionTimeG = Int32.Parse(questionTime);
            questionId++;
            Thread t = new Thread(runTime);
            t.Start();
        }
        public void runTime()
        {
            while(time<questionTimeG)
            {
                timeValueLabel.Text = (questionTimeG-time).ToString();
                Thread.Sleep(1000);
                timeValueLabel.Text = "";
                time++;
            }
            //player didnt answer the question at the time
            if(closeClicked == false)
            {
                if (answer1button.Enabled == true && answer2Button.Enabled == true && answer3Button.Enabled == true && answer4Button.Enabled == true)
                {
                    string message = "2195" + time.ToString().PadLeft(2, '0');
                    Helper h = new Helper(soc);
                    h.send(message);
                    string answer = h.recieve();
                    this.Close();
                }
                else
                    this.Close();
            }
        }

        private void answer1button_Click(object sender, EventArgs e)
        {
            answer1button.Enabled = false;
            answer2Button.Enabled = false;
            answer3Button.Enabled = false;
            answer4Button.Enabled = false;
            int answerTime = time;
            string message = "2191" + time.ToString().PadLeft(2,'0');
            Helper h = new Helper(soc);
            h.send(message);
            string answer = h.recieve();
            if(answer == "1201")
            {
                answer1button.BackColor = Color.Green;
                score++;
                scoreValueLabel.Text = score.ToString();
                MessageBox.Show("You were right\nWaiting for other to finish...\nIf you press ok the window will be closed\nAnd will be come back when everyone is finised");
            }
            else
            {
                answer1button.BackColor = Color.Red;
                MessageBox.Show("You were wrong\nWaiting for other to finish...\nIf you press ok the window will be closed\nAnd will be come back when everyone is finised");
            }
            this.Close();
        }

        private void answer2Button_Click(object sender, EventArgs e)
        {
            answer1button.Enabled = false;
            answer2Button.Enabled = false;
            answer3Button.Enabled = false;
            answer4Button.Enabled = false;
            int answerTime = time;
            string message = "2192" + time.ToString().PadLeft(2, '0');
            Helper h = new Helper(soc);
            h.send(message);
            string answer = h.recieve();
            if (answer == "1201")
            {
                answer2Button.BackColor = Color.Green;
                score++;
                scoreValueLabel.Text = score.ToString();
                MessageBox.Show("You were right\nWaiting for other to finish...\nIf you press ok the window will be closed\nAnd will be come back when everyone is finised");
            }
            else
            {
                answer2Button.BackColor = Color.Red;
                MessageBox.Show("You were wrong\nWaiting for other to finish...\nIf you press ok the window will be closed\nAnd will be come back when everyone is finised");
            }
            this.Close();
        }

        private void answer3Button_Click(object sender, EventArgs e)
        {
            answer1button.Enabled = false;
            answer2Button.Enabled = false;
            answer3Button.Enabled = false;
            answer4Button.Enabled = false;
            int answerTime = time;
            string message = "2193" + time.ToString().PadLeft(2, '0');
            Helper h = new Helper(soc);
            h.send(message);
            string answer = h.recieve();
            if (answer == "1201")
            {
                answer3Button.BackColor = Color.Green;
                score++;
                scoreValueLabel.Text = score.ToString();
                MessageBox.Show("You were right\nWaiting for other to finish...\nIf you press ok the window will be closed\nAnd will be come back when everyone is finised");
            }
            else
            {
                answer3Button.BackColor = Color.Red;
                MessageBox.Show("You were wrong\nWaiting for other to finish...\nIf you press ok the window will be closed\nAnd will be come back when everyone is finised");
            }
            this.Close();
        }

        private void answer4Button_Click(object sender, EventArgs e)
        {
            answer1button.Enabled = false;
            answer2Button.Enabled = false;
            answer3Button.Enabled = false;
            answer4Button.Enabled = false;
            int answerTime = time;
            string message = "2194" + time.ToString().PadLeft(2, '0');
            Helper h = new Helper(soc);
            h.send(message);
            string answer = h.recieve();
            if (answer == "1201")
            {
                answer4Button.BackColor = Color.Green;
                score++;
                scoreValueLabel.Text = score.ToString();
                MessageBox.Show("You were right\nWaiting for other to finish...\nIf you press ok the window will be closed\nAnd will be come back when everyone is finised");
            }
            else
            {
                answer4Button.BackColor = Color.Red;
                MessageBox.Show("You were wrong\nWaiting for other to finish...\nIf you press ok the window will be closed\nAnd will be come back when everyone is finised");
            }
            this.Close();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            closeClicked = true;
            Helper h = new Helper(soc);
            h.send("222");
            this.Close();
        }
    }
}
