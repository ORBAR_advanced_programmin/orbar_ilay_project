#include "User.h"
#include "Room.h"
#include "Helper.h"
#include <string>

using namespace std;

class User;
class Helper;

Room::Room(int id, User * admin, string name, int maxUsers, int questionsNo, int questionTime)
{
	this->_id = id;
	this->_admin = admin;
	this->_name = name;
	this->_maxUsers = maxUsers;
	this->_questionNum = questionsNo;
	this->_questionTime = questionTime;
}

Room::~Room()
{
}

string Room::getUsersAsString()
{
	string all = "";
	for (vector<User*>::iterator i = this->_users.begin(); i != this->_users.end(); i++)
	{
		all += (*i)->getUserName() + " ";
	}
	return all;
}

string Room::getUsersListMessage()
{
	std::stringstream ss;
	string usernameLength;
	string ppl = "108";
	string pplAmount = to_string(this->_users.size());
	ppl += pplAmount;
	for (vector<User*>::iterator i = this->_users.begin(); i != this->_users.end(); i++)
	{
		usernameLength = to_string((*i)->getUserName().length());
		ss << std::setw(2) << std::setfill('0') << usernameLength;
		ppl += ss.str();
		ss.str("");
		ss.clear();
		ppl += (*i)->getUserName();
	}
	return ppl;
}

int Room::getId()
{
	return this->_id;
}

void Room::sendMessage(User * ExcludeUser, string msg)
{
	if (ExcludeUser != nullptr)
	{
		for (vector<User*>::iterator i = this->_users.begin(); i != this->_users.end(); i++)
		{
			if ((ExcludeUser)->getUserName() != (*i)->getUserName())
			{
				Helper::sendData((*i)->getSocket(), msg);
			}
		}
	}
	else
	{
		for (vector<User*>::iterator i = this->_users.begin(); i != this->_users.end(); i++)
		{
			Helper::sendData((*i)->getSocket(), msg);
		}
	}
	
}

void Room::sendMessage(string msg)
{
	sendMessage(NULL, msg);
}

bool Room::joinRoom(User * user)
{
	std::stringstream ss;
	int pplAmount = this->_users.size();
	string ppl = "";
	string answer = "1100";
	cout << pplAmount << endl;
	cout << this->_maxUsers << endl;
	if (pplAmount < this->_maxUsers)
	{
		this->_users.push_back(user);
		ss << std::setw(2) << std::setfill('0') << this->_questionNum;
		answer += ss.str();
		ss.str("");
		ss.clear();
		ss << std::setw(2) << std::setfill('0') << this->_questionTime;
		answer += ss.str();
		ss.str("");
		ss.clear();
		Helper::sendData(user->getSocket(), answer);
		ppl = this->getUsersListMessage();
		sendMessage(NULL, ppl);
		return true;
	}
	else
	{
		Helper::sendData(user->getSocket(), "1101");
		return false;
	}
}

void Room::leaveRoom(User * user)
{
	
	int count = -1;
	for (int i = 0; i < size(this->_users); i++)
	{
		if (user->getUserName() == this->_users[i]->getUserName())
		{
			count = i;
			break;
		}
	}
	//user doesn't exist in the current room
	if (count == -1)
	{
		return;
	}
	this->_users.erase(this->_users.begin() + count);
	Helper::sendData(user->getSocket(), "1120");
	string ppl = this->getUsersListMessage();
	this->sendMessage(NULL, ppl);
}


int Room::closeRoom(User * user)
{
	if (user->getUserName() != this->_admin->getUserName())
	{
		return -1;
	}

	sendMessage(nullptr, "116");
	
	for (vector<User*>::iterator i = this->_users.begin(); i != this->_users.end(); i++)
	{
		((*i))->clearRoom();
	}
	
	return this->_id;
}
string Room::getName()
{
	return this->_name;
}
int Room::getQuestionTime()
{
	return this->_questionTime;
}
int Room::getQuestionNo()
{
	return this->_questionNum;
}
vector<User*> Room::getUsers()
{
	return _users;
}

