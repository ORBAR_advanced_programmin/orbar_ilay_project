#include <algorithm>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include "DataBase.h"
#include "Question.h"
#include <windows.h>
#define SEPERATING_STRING 5
#define AMOUNT_OF_QUESTIONS 10



unordered_map<string, vector<string>> results;
vector<string> best;
vector<string> personalStat;
int countCheck = 0;
int myrandom(int i) { return std::rand() % i; }
vector<string> DataBase::getBestScores()
{
	char* zErrMsg = 0;
	vector<string> bestScores;
	if (results.size() == 0)
	{
		return vector<string>();
	}
	int rc = sqlite3_exec(this->_db, "SELECT * FROM t_players_answers;",callback,0, &zErrMsg);
	if (_rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return vector<string>();
	}
	//printTable();
	cout << "wtf" << endl;
	clearTable();
	this->_rc = sqlite3_exec(this->_db, "select username,count(is_correct),AVG(answer_time) from t_players_answers where is_correct = 1 group by username,game_id order by count(is_correct) desc LIMIT 3;", callbackBestScores, 0, &zErrMsg);
	if (_rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return vector<string>();
	}
	bestScores = best;
	best.clear();
	return bestScores;
}

int getLastInsertId()
{
	if (results.size() == 0)
	{
		return 0;
	}
	int last = 0;
	auto iter = results.end();
	iter--;
	int size = iter->second.size();
	for (int i = 0; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			last = stoi(it->second.at(i));
		}
	}
	return last;
}


/*
This function adds an answer of a player.
Input : all the information for player's answer.
Output : True if the answer registered in the database else, false.
*/
bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	char* zErrMsg = 0;
	string values = quotesql(to_string(gameId))+","+quotesql(username)+","+quotesql(to_string(questionId))+","+quotesql(answer)+","+quotesql(to_string(isCorrect))+","+quotesql(to_string(answerTime));
	this->_rc = sqlite3_exec(this->_db,("INSERT INTO t_players_answers(game_id,username,question_id,player_answer,is_correct,answer_time) values("+values+");").c_str(), callback, 0, &zErrMsg);
	if (this->_rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	return true;
}

int DataBase::callbackPersonalStatus(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;
	string Pers = "";
	for (i = 0; i < argc && argv[i]!=NULL; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			Pers += argv[i] + string(",");
			it->second.push_back(argv[i]);
		}
		else
		{
			Pers += argv[i] + string(",");
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}
	personalStat.push_back(Pers);// first 
	return 0;
}



vector<string> DataBase::getPersonalStatus(string name)
{
	char * zErrMsg = 0;
	vector<string> tmp;
	string tmpS = "";
	clearTable();
	int rc = sqlite3_exec(this->_db, (string("select count(DISTINCT game_id) as participated_games from t_players_answers where username = '" + name + "'; select count(is_correct) as correct_answers from t_players_answers where is_correct = 1 and username = '" + name + "'; select count(is_correct),AVG(answer_time) from t_players_answers where is_correct = 0 and username = '" + name + "';")).c_str(), callbackPersonalStatus, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return vector<string>();
	}
	/*for (vector<string>::iterator i = personalStat.begin(); i != personalStat.end(); i++)
	{
		cout << personalStat[j];
		stringstream ss = stringstream(*i);
		while (ss.str().find(tmpS) + tmpS.length() != ss.str().length())
		{
			getline(ss, tmpS, ',');
			if (tmpS == "")
				break;
			tmp.push_back(tmpS);
		}
		j++;
	}*/
	for (int i = 0; i < personalStat.size(); i++)
	{
		tmpS += personalStat[i];
	}
	//happens when player didnt answer at least one question or player has no game
	if (personalStat.size() < 4)
	{
		tmpS += "0.0,";
	}
	cout << tmpS << endl;
	tmpS[tmpS.find_last_of(',')] = ' ';
	cout << tmpS << endl;
	tmp.push_back(tmpS);
	personalStat.clear();
	return tmp;
}


int DataBase::callbackBestScores(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;
	string perUser = "";
	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
			perUser += argv[i] + string(",");
		}
		else
		{
			perUser += argv[i] + string(",");
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}
	perUser[perUser.find_last_of(',')] = ' ';
	best.push_back(perUser);
	perUser = "";
	return 0;
}


/*
This function checks if the status has changed in the database
Input : id of the game that should have changed
Output : true if changed , else false
*/
bool HasChangedStatus(int id)
{
	if (results.size() == 0)
	{
		return false;
	}
	int idPlace = -1;
	bool hasStatusChanged = false;
	auto iter = results.end();
	iter--;
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (it->first == "id")
			{
				if (it->second.at(i) == to_string(id))
				{
					idPlace = i;
				}
			}
			else if (it->first == "status")
			{
				if (it->second.at(idPlace) == "1")
				{
					hasStatusChanged = true;
				}
			}
		}
	}
	return false;
}

bool IsThereUserAndPass(string name,string pass)
{
	int namePlace = -1,passPlace = -1;

	auto iter = results.end();
	iter--;
	int size = iter->second.size();
	for (int i = 0; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (it->first == "username")
			{
				if (it->second.at(i) == name)
				{
					namePlace = i;
				}
			}
			else if (it->first == "password")
			{
				if (it->second.at(namePlace) == pass)
				{
					passPlace = i;
					break;
				}
			}
		}
	}
	return (passPlace == namePlace);
}

int DataBase::printTable()
{
	if (results.size() == 0)
	{
		return 1;
	}
	auto iter = results.end();
	iter--;
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				printf("|%*s|", 13, it->first.c_str()); // name of the column
			}
			else if (i == -1)
				cout << "_______________"; // drawing line between each line
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str()); // value of each column in specific line
		}
		cout << endl;
	}
	return 0;
}

void DataBase::clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

int DataBase::callbackCount(void * count, int argc, char** argv, char** azColName)
{
	int i;
	fprintf(stderr, "%s: ", (const char*)count);
	for (i = 0; i<argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	countCheck = atoi(argv[0]);
	cout << countCheck << endl;
	return 0;
}
int callback(void * notUsed, int argc, char** argv, char** azCol)
{
	int i;
	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

DataBase::DataBase()
{
	char *zErrMsg = 0;
	//open database
	_rc = sqlite3_open("data.db", &_db);
	if (_rc)
		cout << "Cannot open database" << sqlite3_errmsg(_db) << endl;
	else
		cout << "Opened database successfully" << endl;
	//create table
	_rc = sqlite3_exec(_db, "CREATE table t_users(username TEXT primary key not null,password TEXT not null,email TEXT not null);", callback, 0, &zErrMsg);
	if (_rc != SQLITE_OK) 
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else 
	{
		cout << "Table created successfully\n";
	}

	_rc = sqlite3_exec(_db, "CREATE table t_games(game_id integer primary key autoincrement not null,status integer not null,start_time time not null,end_time time);", callback, 0, &zErrMsg);
	if (_rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else
	{
		cout << "Table created successfully\n";
	}

	_rc = sqlite3_exec(_db, "CREATE table t_questions(question_id integer primary key autoincrement not null,question TEXT not null,correct_ans TEXT not null,ans2 TEXT not null,ans3 TEXT not null,ans4 TEXT not null)", callback, 0, &zErrMsg);
	if (_rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else
	{
		cout << "Table created successfully\n";
	}

	_rc = sqlite3_exec(_db, "CREATE table t_players_answers(game_id integer not null, username TEXT not null,question_id integer not null,player_answer TEXT not null,is_correct integer not null,answer_time integer not null,primary key(game_id,username,question_id),foreign key(game_id) REFERENCES t_games(game_id),foreign key(username) REFERENCES t_users(username),foreign key(question_id) REFERENCES t_questions(question_id))", callback, 0, &zErrMsg);
	if (_rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else
	{
		cout << "Table created successfully\n";
	}

}


DataBase::~DataBase()
{
	this->_rc = sqlite3_close(_db);
}

int DataBase::callback(void * notUsed, int argc, char ** argv, char ** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;

}

bool DataBase::isUserExists(string username)
{
	char* zErrMsg = 0;
	_rc = sqlite3_exec(_db, ("SELECT COUNT(*) from t_users where username = "+ quotesql(username)).c_str(), callbackCount, 0, &zErrMsg);
	if (_rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		return false;
	}
	if (countCheck == 0)
	{
		cout << "User wasnt found..." << endl;
		return false;
	}
	cout << "user was found..." << endl;
	return true;
}

bool DataBase::addNewUser(string username, string password, string email)
{
	char * zErrMsg = 0;
	string sqlstatement =
		"INSERT INTO t_users (username,password,email) VALUES ("
		+ quotesql(username) + ","
		+ quotesql(password) + ","
		+ quotesql(email) + ");";
	_rc = sqlite3_exec(this->_db, sqlstatement.c_str() , callback, 0, &zErrMsg);
	if (_rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		return false;
	}
	else
	{
		cout << "User was added" << endl;
		return true;
	}
}
string DataBase::quotesql(const string& s) 
{
	return string("'") + s + string("'");
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	char* zErrMsg = 0;
	this->_rc = sqlite3_exec(this->_db, ("SELECT COUNT(*) from t_users WHERE username = " + quotesql(username) + " and password = " + quotesql(password)).c_str(), callbackCount, 0, &zErrMsg);
	if (_rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		return false;
	}
	if (countCheck == 0)
	{
		cout << "User wasnt found..." << endl;
		return false;
	}
	cout << "user was found..." << endl;
	return true;
	//int thereIs;
	//clearTable();
	//bool ans = IsThereUserAndPass(username, password);
	//clearTable();
	//this->_rc = sqlite3_exec(this->_db,("SELECT username,password from t_users WHERE username = "+username+" and password = "+password).c_str(), callback, 0, &zErrMsg);
	//if (_rc != SQLITE_OK)
	//{
	//	return false;

	//}
	//thereIs = printTable();
	//if (thereIs == 1)
	//{
	//	return false;
	//}
	//return true;
	
}

vector<Question*> DataBase::initQuestions(int questionNum)
{
	vector<Question*> ques;
	string path = ExePath() + "\\Questions.txt";
	cout << path << endl;
	
	ifstream file(path);
	if (!file.is_open())
	{
		cout << "Cannot open file" << endl;
		return ques;
	}

	string one = "";
	vector<string> allQue;
	string line = "";
	cout << "Opened.." << endl;
	int count = 0;
	string question[5];
	int id = 1;
	while (getline(file,line))
	{
		if (line == "")
		{
			cout << "next question" << endl;
			ques.push_back(new Question(id, question[0], question[1], question[2], question[3],question[4]));
			id++;
			count = 0;
		}
		else
		{
			question[count] = line;
			count++;
		}
	}
	file.close();
	random_shuffle(ques.begin(), ques.end());
	random_shuffle(ques.begin(), ques.end(),myrandom);
	for (int i = 0; i < size(ques); i++)
	{
		cout << ques[i]->getQuestion() << endl;
	}
	vector<Question*> random_ques;
	for (int i = 0; i < size(ques) && i < questionNum; i++)
	{
		random_ques.push_back(ques[i]);
	}
	cout << "Done" << endl;
	return random_ques;
}

bool DataBase::UpdateGameStatus(int id)
{
	char* zErrMsg = 0;
	bool hasUpdated = false;
	this->_rc = sqlite3_exec(this->_db, ("UPDATE t_games set status = 1 ,end_time = julianday('now') where game_id =" + to_string(id)).c_str(),callback,0,&zErrMsg);
	if (this->_rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return hasUpdated;
	}

	return hasUpdated;
}

int DataBase::insertNewGame()
{
	char* zErrMsg = 0;
	string values = "values(0,julianday('now'));";
	this->_rc = sqlite3_exec(this->_db,("INSERT into t_games(status,start_time) " + values).c_str(), callback, 0, &zErrMsg);
	if (this->_rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return -1;
	}
	clearTable();
	this->_rc = sqlite3_exec(this->_db, "SELECT game_id from t_games", callback, 0, &zErrMsg);
	if (this->_rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return -1;
	}
	int last = getLastInsertId();
	return last;
}
string DataBase::ExePath() 
{
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	string::size_type pos = string(buffer).find_last_of("\\/");
	return string(buffer).substr(0, pos);
}

