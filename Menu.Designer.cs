﻿namespace TriviaClient
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usernameBox = new System.Windows.Forms.TextBox();
            this.passwordBox = new System.Windows.Forms.TextBox();
            this.signinButton = new System.Windows.Forms.Button();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.signupbutton = new System.Windows.Forms.Button();
            this.Signoutbutton = new System.Windows.Forms.Button();
            this.joinroombutton = new System.Windows.Forms.Button();
            this.createroombutton = new System.Windows.Forms.Button();
            this.personalstatusbutton = new System.Windows.Forms.Button();
            this.bestscoresbutton = new System.Windows.Forms.Button();
            this.quitbutton = new System.Windows.Forms.Button();
            this.connectedNameLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.errorLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // usernameBox
            // 
            this.usernameBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.usernameBox.Location = new System.Drawing.Point(179, 92);
            this.usernameBox.Margin = new System.Windows.Forms.Padding(2);
            this.usernameBox.Name = "usernameBox";
            this.usernameBox.Size = new System.Drawing.Size(279, 30);
            this.usernameBox.TabIndex = 0;
            this.usernameBox.Text = "user";
            // 
            // passwordBox
            // 
            this.passwordBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.passwordBox.Location = new System.Drawing.Point(179, 140);
            this.passwordBox.Margin = new System.Windows.Forms.Padding(2);
            this.passwordBox.Name = "passwordBox";
            this.passwordBox.Size = new System.Drawing.Size(279, 30);
            this.passwordBox.TabIndex = 1;
            this.passwordBox.Text = "Aa12";
            // 
            // signinButton
            // 
            this.signinButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.signinButton.Location = new System.Drawing.Point(493, 108);
            this.signinButton.Margin = new System.Windows.Forms.Padding(2);
            this.signinButton.Name = "signinButton";
            this.signinButton.Size = new System.Drawing.Size(88, 35);
            this.signinButton.TabIndex = 2;
            this.signinButton.Text = "Sign in";
            this.signinButton.UseVisualStyleBackColor = true;
            this.signinButton.Click += new System.EventHandler(this.signinButton_Click);
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.usernameLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.usernameLabel.Location = new System.Drawing.Point(50, 95);
            this.usernameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(113, 25);
            this.usernameLabel.TabIndex = 3;
            this.usernameLabel.Text = "Username: ";
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.passwordLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.passwordLabel.Location = new System.Drawing.Point(50, 145);
            this.passwordLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(109, 25);
            this.passwordLabel.TabIndex = 4;
            this.passwordLabel.Text = "Password: ";
            // 
            // signupbutton
            // 
            this.signupbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.signupbutton.Location = new System.Drawing.Point(243, 217);
            this.signupbutton.Name = "signupbutton";
            this.signupbutton.Size = new System.Drawing.Size(128, 33);
            this.signupbutton.TabIndex = 5;
            this.signupbutton.Text = "Sign up";
            this.signupbutton.UseVisualStyleBackColor = true;
            this.signupbutton.Click += new System.EventHandler(this.signupbutton_Click);
            // 
            // Signoutbutton
            // 
            this.Signoutbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Signoutbutton.Location = new System.Drawing.Point(243, 256);
            this.Signoutbutton.Name = "Signoutbutton";
            this.Signoutbutton.Size = new System.Drawing.Size(128, 33);
            this.Signoutbutton.TabIndex = 6;
            this.Signoutbutton.Text = "Sign out";
            this.Signoutbutton.UseVisualStyleBackColor = true;
            this.Signoutbutton.Click += new System.EventHandler(this.Signoutbutton_Click);
            // 
            // joinroombutton
            // 
            this.joinroombutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.joinroombutton.Location = new System.Drawing.Point(243, 295);
            this.joinroombutton.Name = "joinroombutton";
            this.joinroombutton.Size = new System.Drawing.Size(128, 35);
            this.joinroombutton.TabIndex = 7;
            this.joinroombutton.Text = "Join room";
            this.joinroombutton.UseVisualStyleBackColor = true;
            this.joinroombutton.Click += new System.EventHandler(this.joinroombutton_Click);
            // 
            // createroombutton
            // 
            this.createroombutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.createroombutton.Location = new System.Drawing.Point(243, 336);
            this.createroombutton.Name = "createroombutton";
            this.createroombutton.Size = new System.Drawing.Size(128, 37);
            this.createroombutton.TabIndex = 8;
            this.createroombutton.Text = "Create room";
            this.createroombutton.UseVisualStyleBackColor = true;
            this.createroombutton.Click += new System.EventHandler(this.createroombutton_Click);
            // 
            // personalstatusbutton
            // 
            this.personalstatusbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.personalstatusbutton.Location = new System.Drawing.Point(243, 379);
            this.personalstatusbutton.Name = "personalstatusbutton";
            this.personalstatusbutton.Size = new System.Drawing.Size(128, 35);
            this.personalstatusbutton.TabIndex = 9;
            this.personalstatusbutton.Text = "My status";
            this.personalstatusbutton.UseVisualStyleBackColor = true;
            this.personalstatusbutton.Click += new System.EventHandler(this.personalstatusbutton_Click);
            // 
            // bestscoresbutton
            // 
            this.bestscoresbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.bestscoresbutton.Location = new System.Drawing.Point(243, 420);
            this.bestscoresbutton.Name = "bestscoresbutton";
            this.bestscoresbutton.Size = new System.Drawing.Size(128, 33);
            this.bestscoresbutton.TabIndex = 10;
            this.bestscoresbutton.Text = "Best scores";
            this.bestscoresbutton.UseVisualStyleBackColor = true;
            this.bestscoresbutton.Click += new System.EventHandler(this.bestscoresbutton_Click);
            // 
            // quitbutton
            // 
            this.quitbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.quitbutton.Location = new System.Drawing.Point(2, 2);
            this.quitbutton.Name = "quitbutton";
            this.quitbutton.Size = new System.Drawing.Size(104, 36);
            this.quitbutton.TabIndex = 11;
            this.quitbutton.Text = "Quit";
            this.quitbutton.UseVisualStyleBackColor = true;
            this.quitbutton.Click += new System.EventHandler(this.quitbutton_Click);
            // 
            // connectedNameLabel
            // 
            this.connectedNameLabel.AutoSize = true;
            this.connectedNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.connectedNameLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.connectedNameLabel.Location = new System.Drawing.Point(248, 154);
            this.connectedNameLabel.Name = "connectedNameLabel";
            this.connectedNameLabel.Size = new System.Drawing.Size(123, 54);
            this.connectedNameLabel.TabIndex = 12;
            this.connectedNameLabel.Text = "User";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::TriviaClient.Properties.Resources.rsz_trivia_1;
            this.pictureBox1.Location = new System.Drawing.Point(134, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(392, 81);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.errorLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.errorLabel.Location = new System.Drawing.Point(239, 182);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(48, 20);
            this.errorLabel.TabIndex = 14;
            this.errorLabel.Text = "Error ";
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuText;
            this.ClientSize = new System.Drawing.Size(716, 483);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.connectedNameLabel);
            this.Controls.Add(this.quitbutton);
            this.Controls.Add(this.bestscoresbutton);
            this.Controls.Add(this.personalstatusbutton);
            this.Controls.Add(this.createroombutton);
            this.Controls.Add(this.joinroombutton);
            this.Controls.Add(this.Signoutbutton);
            this.Controls.Add(this.signupbutton);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.signinButton);
            this.Controls.Add(this.passwordBox);
            this.Controls.Add(this.usernameBox);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Menu";
            this.Text = "Menu";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox usernameBox;
        private System.Windows.Forms.TextBox passwordBox;
        private System.Windows.Forms.Button signinButton;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.Button signupbutton;
        private System.Windows.Forms.Button Signoutbutton;
        private System.Windows.Forms.Button joinroombutton;
        private System.Windows.Forms.Button createroombutton;
        private System.Windows.Forms.Button personalstatusbutton;
        private System.Windows.Forms.Button bestscoresbutton;
        private System.Windows.Forms.Button quitbutton;
        private System.Windows.Forms.Label connectedNameLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label errorLabel;
    }
}

