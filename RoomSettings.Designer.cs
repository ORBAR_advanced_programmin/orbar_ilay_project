﻿namespace TriviaClient
{
    partial class RoomSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.roomName = new System.Windows.Forms.RichTextBox();
            this.playersN = new System.Windows.Forms.RichTextBox();
            this.questionsN = new System.Windows.Forms.RichTextBox();
            this.questionsT = new System.Windows.Forms.RichTextBox();
            this.roomNameLabel = new System.Windows.Forms.Label();
            this.playersNumberLabel = new System.Windows.Forms.Label();
            this.questionsNumberLabel = new System.Windows.Forms.Label();
            this.questionTimeLabel = new System.Windows.Forms.Label();
            this.Sendbutton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.errorLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // roomName
            // 
            this.roomName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.roomName.Location = new System.Drawing.Point(232, 81);
            this.roomName.Name = "roomName";
            this.roomName.Size = new System.Drawing.Size(280, 35);
            this.roomName.TabIndex = 0;
            this.roomName.Text = "room_name";
            // 
            // playersN
            // 
            this.playersN.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.playersN.Location = new System.Drawing.Point(232, 132);
            this.playersN.Name = "playersN";
            this.playersN.Size = new System.Drawing.Size(280, 35);
            this.playersN.TabIndex = 1;
            this.playersN.Text = "5";
            // 
            // questionsN
            // 
            this.questionsN.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.questionsN.Location = new System.Drawing.Point(232, 174);
            this.questionsN.Name = "questionsN";
            this.questionsN.Size = new System.Drawing.Size(280, 35);
            this.questionsN.TabIndex = 2;
            this.questionsN.Text = "5";
            // 
            // questionsT
            // 
            this.questionsT.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.questionsT.Location = new System.Drawing.Point(232, 225);
            this.questionsT.Name = "questionsT";
            this.questionsT.Size = new System.Drawing.Size(280, 35);
            this.questionsT.TabIndex = 3;
            this.questionsT.Text = "10";
            // 
            // roomNameLabel
            // 
            this.roomNameLabel.AutoSize = true;
            this.roomNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.roomNameLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.roomNameLabel.Location = new System.Drawing.Point(38, 80);
            this.roomNameLabel.Name = "roomNameLabel";
            this.roomNameLabel.Size = new System.Drawing.Size(122, 25);
            this.roomNameLabel.TabIndex = 4;
            this.roomNameLabel.Text = "room_name:";
            // 
            // playersNumberLabel
            // 
            this.playersNumberLabel.AutoSize = true;
            this.playersNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.playersNumberLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.playersNumberLabel.Location = new System.Drawing.Point(38, 142);
            this.playersNumberLabel.Name = "playersNumberLabel";
            this.playersNumberLabel.Size = new System.Drawing.Size(173, 25);
            this.playersNumberLabel.TabIndex = 5;
            this.playersNumberLabel.Text = "number of players:";
            // 
            // questionsNumberLabel
            // 
            this.questionsNumberLabel.AutoSize = true;
            this.questionsNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.questionsNumberLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.questionsNumberLabel.Location = new System.Drawing.Point(38, 184);
            this.questionsNumberLabel.Name = "questionsNumberLabel";
            this.questionsNumberLabel.Size = new System.Drawing.Size(194, 25);
            this.questionsNumberLabel.TabIndex = 6;
            this.questionsNumberLabel.Text = "number of questions:";
            // 
            // questionTimeLabel
            // 
            this.questionTimeLabel.AutoSize = true;
            this.questionTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.questionTimeLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.questionTimeLabel.Location = new System.Drawing.Point(38, 235);
            this.questionTimeLabel.Name = "questionTimeLabel";
            this.questionTimeLabel.Size = new System.Drawing.Size(160, 25);
            this.questionTimeLabel.TabIndex = 7;
            this.questionTimeLabel.Text = "time for question:";
            // 
            // Sendbutton
            // 
            this.Sendbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Sendbutton.Location = new System.Drawing.Point(311, 306);
            this.Sendbutton.Name = "Sendbutton";
            this.Sendbutton.Size = new System.Drawing.Size(86, 30);
            this.Sendbutton.TabIndex = 8;
            this.Sendbutton.Text = "Send";
            this.Sendbutton.UseVisualStyleBackColor = true;
            this.Sendbutton.Click += new System.EventHandler(this.Sendbutton_Click);
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.backButton.Location = new System.Drawing.Point(666, 12);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(86, 30);
            this.backButton.TabIndex = 9;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.errorLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.errorLabel.Location = new System.Drawing.Point(306, 268);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(54, 25);
            this.errorLabel.TabIndex = 10;
            this.errorLabel.Text = "Error";
            // 
            // RoomSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.Sendbutton);
            this.Controls.Add(this.questionTimeLabel);
            this.Controls.Add(this.questionsNumberLabel);
            this.Controls.Add(this.playersNumberLabel);
            this.Controls.Add(this.roomNameLabel);
            this.Controls.Add(this.questionsT);
            this.Controls.Add(this.questionsN);
            this.Controls.Add(this.playersN);
            this.Controls.Add(this.roomName);
            this.Name = "RoomSettings";
            this.Text = "RoomSettings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox roomName;
        private System.Windows.Forms.RichTextBox playersN;
        private System.Windows.Forms.RichTextBox questionsN;
        private System.Windows.Forms.RichTextBox questionsT;
        private System.Windows.Forms.Label roomNameLabel;
        private System.Windows.Forms.Label playersNumberLabel;
        private System.Windows.Forms.Label questionsNumberLabel;
        private System.Windows.Forms.Label questionTimeLabel;
        private System.Windows.Forms.Button Sendbutton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Label errorLabel;
    }
}