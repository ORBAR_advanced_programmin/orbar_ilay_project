﻿namespace TriviaClient
{
    partial class Lobby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.roomNameLabel = new System.Windows.Forms.Label();
            this.headLineLabel = new System.Windows.Forms.Label();
            this.detailsLabel = new System.Windows.Forms.Label();
            this.closeroombutton = new System.Windows.Forms.Button();
            this.startgamebutton = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.leaveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // roomNameLabel
            // 
            this.roomNameLabel.AutoSize = true;
            this.roomNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.roomNameLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.roomNameLabel.Location = new System.Drawing.Point(334, 40);
            this.roomNameLabel.Name = "roomNameLabel";
            this.roomNameLabel.Size = new System.Drawing.Size(116, 25);
            this.roomNameLabel.TabIndex = 0;
            this.roomNameLabel.Text = "room_name";
            // 
            // headLineLabel
            // 
            this.headLineLabel.AutoSize = true;
            this.headLineLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.headLineLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.headLineLabel.Location = new System.Drawing.Point(264, 90);
            this.headLineLabel.Name = "headLineLabel";
            this.headLineLabel.Size = new System.Drawing.Size(306, 25);
            this.headLineLabel.TabIndex = 1;
            this.headLineLabel.Text = "You are connected to room_name";
            // 
            // detailsLabel
            // 
            this.detailsLabel.AutoSize = true;
            this.detailsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.detailsLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.detailsLabel.Location = new System.Drawing.Point(112, 126);
            this.detailsLabel.Name = "detailsLabel";
            this.detailsLabel.Size = new System.Drawing.Size(183, 25);
            this.detailsLabel.TabIndex = 2;
            this.detailsLabel.Text = "number:1 number:2";
            // 
            // closeroombutton
            // 
            this.closeroombutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.closeroombutton.Location = new System.Drawing.Point(339, 355);
            this.closeroombutton.Name = "closeroombutton";
            this.closeroombutton.Size = new System.Drawing.Size(115, 46);
            this.closeroombutton.TabIndex = 4;
            this.closeroombutton.Text = "Close Game";
            this.closeroombutton.UseVisualStyleBackColor = true;
            this.closeroombutton.Click += new System.EventHandler(this.closeroombutton_Click);
            // 
            // startgamebutton
            // 
            this.startgamebutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.startgamebutton.Location = new System.Drawing.Point(339, 407);
            this.startgamebutton.Name = "startgamebutton";
            this.startgamebutton.Size = new System.Drawing.Size(115, 42);
            this.startgamebutton.TabIndex = 5;
            this.startgamebutton.Text = "Start Game";
            this.startgamebutton.UseVisualStyleBackColor = true;
            this.startgamebutton.Click += new System.EventHandler(this.startgamebutton_Click);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 25;
            this.listBox1.Location = new System.Drawing.Point(318, 172);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(151, 129);
            this.listBox1.TabIndex = 6;
            // 
            // leaveButton
            // 
            this.leaveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.leaveButton.Location = new System.Drawing.Point(339, 312);
            this.leaveButton.Name = "leaveButton";
            this.leaveButton.Size = new System.Drawing.Size(115, 37);
            this.leaveButton.TabIndex = 7;
            this.leaveButton.Text = "Leave";
            this.leaveButton.UseVisualStyleBackColor = true;
            this.leaveButton.Click += new System.EventHandler(this.leaveButton_Click);
            // 
            // Lobby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.leaveButton);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.startgamebutton);
            this.Controls.Add(this.closeroombutton);
            this.Controls.Add(this.detailsLabel);
            this.Controls.Add(this.headLineLabel);
            this.Controls.Add(this.roomNameLabel);
            this.Name = "Lobby";
            this.Text = "Lobby";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label roomNameLabel;
        private System.Windows.Forms.Label headLineLabel;
        private System.Windows.Forms.Label detailsLabel;
        private System.Windows.Forms.Button closeroombutton;
        private System.Windows.Forms.Button startgamebutton;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button leaveButton;
    }
}