﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace TriviaClient
{
    public partial class Menu : Form
    {
        public  Socket soc;
        public  string usernameIn;
        public Menu()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            Signoutbutton.Enabled = false;
            joinroombutton.Enabled = false;
            createroombutton.Enabled = false;
            personalstatusbutton.Enabled = false;
            bestscoresbutton.Enabled = false;
            connectedNameLabel.Hide();
            errorLabel.Hide();
            serverContact();
        }
        public void serverContact()
        {
            bool cancelCliked = false;
            soc = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            StreamReader sr = new StreamReader("config.txt");
            string info = sr.ReadToEnd();
            string serverIp = info.Split('\n')[0].Split('=')[1];
            serverIp = serverIp.Replace("\r", "");
            string port = info.Split('\n')[1].Split('=')[1];
            port = port.Replace("\r", "");
            bool Encrypt = false;
            if (info.Split('\n')[2].Split('=')[1].Replace("\r","") == "false")
                Encrypt = false;
            else
                Encrypt = true;
            IPAddress ipAdd = IPAddress.Parse(serverIp);
            IPEndPoint remoteEP = new IPEndPoint(ipAdd, Int32.Parse(port));
            while (true)
            {
                try
                {
                    soc.Connect(remoteEP);
                    break;
                }
                catch(Exception)
                {
                    //Unable to connect
                    DialogResult dialogResult = MessageBox.Show("Unable to connect...\n\nOk - try again\nCancel - exit", "Error", MessageBoxButtons.OKCancel);
                    if (dialogResult == DialogResult.Cancel)
                    {
                        cancelCliked = true;
                        break;
                    }
                }
            }
            if (cancelCliked)
                this.Close();
        }

        private void signinButton_Click(object sender, EventArgs e)
        {  
            string username = usernameBox.Text;
            string password = passwordBox.Text;
            usernameIn = username;
            string message = "200" + username.Length.ToString().PadLeft(2, '0') + username + password.Length.ToString().PadLeft(2, '0') + password;
            Helper h = new Helper(soc);
            h.send(message);
            string answer = h.recieve();
            if (answer == "1020")
            {
                errorLabel.Hide();
                signinButton.Hide();
                usernameBox.Hide();
                passwordBox.Hide();
                signupbutton.Hide();
                usernameLabel.Hide();
                passwordLabel.Hide();
                Signoutbutton.Enabled = true;
                joinroombutton.Enabled = true;
                createroombutton.Enabled = true;
                personalstatusbutton.Enabled = true;
                bestscoresbutton.Enabled = true;
                connectedNameLabel.Show();
                connectedNameLabel.Text = username;
            }
            else if (answer == "1021")
            {
                errorLabel.Show();
                errorLabel.Text = "Wrong details";
            }

            else//1022 
            {
                errorLabel.Show();
                errorLabel.Text = "User is already connected";
            }
        }
    
        private void signupbutton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form signup = new Signup(soc);
            signup.ShowDialog();
            this.Show();
        }

        private void Signoutbutton_Click(object sender, EventArgs e)
        {
            Helper h = new Helper(soc);
            h.send("201");
            signupbutton.Show();
            signinButton.Show();
            usernameBox.Show();
            passwordBox.Show();
            usernameLabel.Show();
            passwordLabel.Show();
            Signoutbutton.Enabled = false;
            joinroombutton.Enabled = false;
            createroombutton.Enabled = false;
            personalstatusbutton.Enabled = false;
            bestscoresbutton.Enabled = false;
            connectedNameLabel.Hide();
        }

        private void quitbutton_Click(object sender, EventArgs e)
        {
            //Helper h = new Helper(soc);
            //h.send("201");
            //socket discconected,server auto detected it...
            this.Close();
        }
        private void createroombutton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form rs = new RoomSettings(soc,usernameIn);
            rs.ShowDialog();
            this.Show();
        }

        private void personalstatusbutton_Click(object sender, EventArgs e)
        {
            Helper h = new Helper(soc);
            h.send("225");
            string answer = h.recieve();
            string msgCode = h.get_part(ref answer, 3);
            string gamesN = Int32.Parse(h.get_part(ref answer, 4)).ToString();
            string correctA = Int32.Parse(h.get_part(ref answer, 6)).ToString();
            string wrongA = Int32.Parse(h.get_part(ref answer, 6)).ToString();
            string number1 = Int32.Parse(h.get_part(ref answer, 2)).ToString();
            string number2 = Int32.Parse(h.get_part(ref answer, 2)).ToString();
            string number = number1 + "." + number2;
            this.Hide();
            Form ps = new PersonalStatus(soc, gamesN, correctA, wrongA, number);
            ps.ShowDialog();
            this.Show();
        }

        private void bestscoresbutton_Click(object sender, EventArgs e)
        {
            Helper h = new Helper(soc);
            h.send("223");
            string answer = h.recieve();
            string msgCode = h.get_part(ref answer, 3);
            string length;
            string username;
            string score;
            string[] users = new string[3];
            string[] scores = new string[3];
            int i = 0;
            while(answer!="")
            {
                length = h.get_part(ref answer, 2);
                username = h.get_part(ref answer, Int32.Parse(length));
                score = Int32.Parse(h.get_part(ref answer, 6)).ToString();
                users[i] = username;
                scores[i] = score;
                i++;
            }
            this.Hide();
            Form bs = new BestScores(users, scores);
            bs.ShowDialog();
            this.Show();
        }

        private void joinroombutton_Click(object sender, EventArgs e)
        {
            Helper h = new Helper(soc);
            h.send("205");
            this.Hide();
            Form jr = new Joinroom(soc,usernameIn);
            jr.ShowDialog();
            this.Show();
        }
    }
}
