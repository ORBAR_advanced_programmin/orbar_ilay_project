#include "Protocol.h"
#include "Game.h"
#include <map>
#include <vector>

class DataBase;



Game::Game(int id,DataBase & db, int questionNo, const vector<User*>& players):_db(db)
{
	this->_question_no = questionNo;
	this->_players = players;
	this->_id = id;
	this->_currQuestionIndex = 0;
	this->_currentTurnAnswers = 0;
	this->_db.insertNewGame();
	this->_questions = this->_db.initQuestions(questionNo);
	for (int i = 0; i < size(this->_players); i++)
	{
		this->_players[i]->setGame(this);
		this->_results[this->_players[i]->getUserName()] = 0;
	}
}

Game::~Game()
{
	this->_questions.clear();
	this->_players.clear();
	this->_results.clear();
}

/*
This function sends to all users a message.
Input : None.
Output : None.
*/
void Game::sendQuestionToAllUsers()
{
	this->_currentTurnAnswers = 0;
	string answer = "118";
	std::stringstream ss;
	Question* q = this->_questions[this->_currQuestionIndex];
	string questionLen = to_string(q->getQuestion().length());
	ss << std::setw(3) << std::setfill('0') << questionLen;
	answer += ss.str();
	ss.str("");
	ss.clear();
	answer += q->getQuestion();
	string answerLen = "";
	for (int i = 0; i < 4; i++)
	{
		answerLen = to_string(q->getAnswers()[i].length());
		ss << std::setw(3) << std::setfill('0') << answerLen;
		answer += ss.str();
		ss.str("");
		ss.clear();
		answer += q->getAnswers()[i];
	}
	cout << answer << endl;
	vector<User*>::iterator i;
	try
	{
		for (i = this->_players.begin(); i != this->_players.end(); i++)
		{
			(*i)->send(answer);
		}
	}
	catch (...)
	{
		while (i != this->_players.end())
		{
			i++;
			(*i)->send(answer);
		}
	}
}

void Game::handleFinishGame()
{
	this->_db.UpdateGameStatus(this->_id);
	stringstream ss;
	string answer = "121";
	string usersLength = to_string(size(this->_players));
	answer += usersLength;
	string userLength;
	string score;
	for (int i = 0; i < size(this->_players); i++)
	{
		userLength = to_string(this->_players[i]->getUserName().length());
		ss << std::setw(2) << std::setfill('0') << userLength;
		answer += ss.str();
		ss.str("");
		ss.clear();
		answer += this->_players[i]->getUserName();
		score = to_string(this->_results[this->_players[i]->getUserName()]);
		ss << std::setw(2) << std::setfill('0') << score;
		answer += ss.str();
		ss.str("");
		ss.clear();
	}
	cout << answer << endl;
	vector<User*>::iterator i;
	try
	{
		for (i = this->_players.begin(); i != this->_players.end(); i++)
		{
			(*i)->setGame(nullptr);
			(*i)->send(answer);
		}
	}
	catch (...)
	{
		while (i != this->_players.end())
		{
			i++;
			(*i)->send(answer);
		}
	}

}

bool Game::handleNextTurn()
{
	//All the players already answer
	if (this->_currentTurnAnswers == size(this->_players))
	{
		cout << size(this->_questions) << endl;
		cout << this->_currQuestionIndex + 1 << endl;
		if (this->_currQuestionIndex + 1 == size(this->_questions) || this->_players.size() == 0)
		{
			cout << "Game is over..." << endl;
			this->handleFinishGame();
			return false;
		}
		cout << "Game continues" << endl;
		this->_currQuestionIndex++;
		this->sendQuestionToAllUsers();
		return true;
	}
	return true;
}

void Game::sendFirstQuestion()
{
	this->sendQuestionToAllUsers();
}

/*
This function handles the answers that recieved from the user.
Input : user's info(User *user), his answer(answerNo) and the time until he answered the question(time)
Output : True if the game continues ,else false.
*/
bool Game::handleAnswerFromUser(User * user, int answerNo, int time)
{
	this->_currentTurnAnswers++;
	Question* q = this->_questions[this->_currQuestionIndex];
	//time is over or answer isnt right,if time is over the answer is 5
	if (answerNo != q->getCorrectAnswerIndex()+1)
	{
		cout << "Wrong answer" << endl;
		string answer = to_string(answerNo);
		if (answerNo == 5)
			answer = "";
		this->_db.addAnswerToPlayer(this->_id, user->getUserName(), this->_currQuestionIndex, answer, false, time);
		Helper::sendData(user->getSocket(), "1200");
		return this->handleNextTurn();
	}
	cout << "Correct answer" << endl;
	string answer = to_string(answerNo);
	this->_db.addAnswerToPlayer(this->_id, user->getUserName(), this->_currQuestionIndex, answer, true, time);
	this->_results[user->getUserName()]++;
	Helper::sendData(user->getSocket(), "1201");
	return this->handleNextTurn();
}
int Game::getId()
{
	return this->_id;
}
bool Game::leaveGame(User* user)
{
	int count = -1;
	for (int i = 0; i < size(this->_players); i++)
	{
		if (this->_players[i]->getUserName() == user->getUserName())
		{
			count = i;
		}
	}
	//user wasnt found
	if (count == -1)
	{
		return false;
	}
	this->_players.erase(this->_players.begin() + count);
	this->handleNextTurn();
	return true;
}
