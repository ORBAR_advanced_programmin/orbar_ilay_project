#include "Validator.h"



Validator::Validator()
{
}


Validator::~Validator()
{
}
bool Validator::isPasswordValid(string password)
{
	cout << "Checking password..." << endl;
	if (password.length() < 4)
		return false;
	if (password.find(" ") != std::string::npos)
		return false;
	int count_digit = 0;
	int count_upper = 0;
	int count_lower = 0;
	for (int i = 0; i < password.length(); i++)
	{
		if (password[i] >= '0' && password[i] <= '9')
		{
			count_digit++;
		}
		if (password[i] >= 'a' && password[i] <= 'z')
		{
			count_lower++;
		}
		if (password[i] >= 'A'  && password[i] <= 'Z')
		{
			count_upper++;
		}
	}
	if (count_digit == 0 || count_upper == 0 || count_lower == 0)
	{
		return false;
	}
	return true;
}
bool Validator::isUsernameValid(string username)
{
	cout << "Checking username..." << endl;
	if (username[0] < 'A' || (username[0] > 'Z' && username[0] < 'a') || username[0]>'z')
		return false;
	if (username.find(" ") != std::string::npos)
		return false;
	if (username.length() == 0)
		return false;
	return true;
}
