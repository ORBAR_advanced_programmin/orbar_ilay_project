#include <string>
#include <WinSock2.h>
#include "User.h"
#include "Room.h"
#include "Protocol.h"
#include "Helper.h"

using namespace std;

class Room;
class Helper;

User::User(SOCKET sock, string username)
{
	_sock = sock;
	_username = username;
	_currRoom = NULL;
	_currGame = NULL;
}



User::~User()
{
}

void User::send(string toSend)
{
	Helper::sendData(_sock, toSend);
}

string User::getUserName()
{
	return _username;
}

SOCKET User::getSocket()
{
	return _sock;
}

Room * User::getRoom()
{
	return _currRoom;
}

Game * User::getGame()
{
	return _currGame;
}

void User::setGame(Game * gm)
{
	_currGame = gm;
}

void User::clearRoom()
{
	_currRoom = nullptr;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionsTime)
{
	if (_currRoom != NULL)
	{
		Helper::sendData(_sock, to_string(NEW_ROOM_ANSWER) + "1");
		return false;
	}
	Room *tmp = new Room(roomId, this, roomName, maxUsers, questionsNo, questionsTime);
	_currRoom = tmp;
	_currRoom->_users.push_back(this);
	Helper::sendData(_sock, to_string(NEW_ROOM_ANSWER) + "0");
	cout << "Succeed" << endl;
	return true;
}

bool User::joinRoom(Room * newRoom)
{
	if (_currRoom!= NULL)
	{
		return false;
	}
	bool sign = newRoom->joinRoom(this);
	if (sign)
	{
		_currRoom = newRoom;
		return true;
	}
	else
	{
		return false;
	}
}

void User::leaveRoom()
{
	if (_currRoom != nullptr)
	{
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	int room_id;
	if (_currRoom == nullptr)
	{
		return -1;
	}
	room_id = _currRoom->getId();
	_currRoom->closeRoom(this);
	_currRoom = nullptr;
	return room_id;
}
bool User::leaveGame()
{
	if (this->_currGame != nullptr)
	{
		if (this->_currGame->leaveGame(this))
			return false;
	}
	this->_currGame = nullptr;
	return true;
}
void User::setRoom(Room* r)
{
	this->_currRoom = r;
}
