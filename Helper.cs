﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
namespace TriviaClient
{
    class Helper
    {
        private Socket soc;
        public Helper(Socket soc)
        {
             this.soc = soc;
        }
        public void send(string message)
        {
            byte[] data = Encoding.ASCII.GetBytes(message);
            soc.Send(data);
        }
        public string recieve_part(int size)
        {
            byte[] buffer = new byte[1024];
            int iRx = soc.Receive(buffer, 0, size, 0);
            char[] chars = new char[iRx];
            Decoder d = Encoding.UTF8.GetDecoder();
            int charLen = d.GetChars(buffer, 0, iRx, chars, 0);
            System.String recv = new System.String(chars);
            return recv;
        }

        public string recieve()
        {
            byte[] buffer = new byte[1024];
            int iRx = soc.Receive(buffer);
            char[] chars = new char[iRx];
            Decoder d = Encoding.UTF8.GetDecoder();
            int charLen = d.GetChars(buffer, 0, iRx, chars, 0);
            System.String recv = new System.String(chars);
            return recv;
        }

        public string convertWithBytes(string str,int byts)
        {
            return str.PadLeft(byts,'0');
        }

        public string get_part(ref string str, int size)
        {
            string part = str.Substring(0, size);
            str = str.Substring(size);
            return part;
        }
    }
}
