#include "Question.h"
#include <algorithm>
#include <vector>

Question::Question(int id, string question, string correntAnswer, string answer2, string answer3, string answer4)
{
	this->_id = id;
	this->_question = question;
	vector<string> ans;
	ans.push_back(correntAnswer);
	ans.push_back(answer2);
	ans.push_back(answer3);
	ans.push_back(answer4);
	random_shuffle(ans.begin(),ans.end());
	this->_answers[0] = ans[0];
	this->_answers[1] = ans[1];
	this->_answers[2] = ans[2];
	this->_answers[3] = ans[3];
	for (int i = 0; i < size(this->_answers); i++)
	{
		if (this->_answers[i] == correntAnswer)
			this->_correctAnswerIndex = i;
	}
}


Question::~Question()
{
}

int Question::getId()
{
	return this->_id;
}

string * Question::getAnswers()
{
	return this->_answers;
}

string Question::getQuestion()
{
	return this->_question;
}

int Question::getCorrectAnswerIndex()
{
	return this->_correctAnswerIndex;
}
