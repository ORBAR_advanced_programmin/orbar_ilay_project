﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class BestScores : Form
    {
        public BestScores(string[] users,string[] scores)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            bestScoresBox.Text = "";
            for(int i = 0;i<3;i++)
            {
                bestScoresBox.Text += users[i] + " - " + scores[i] + "\n";
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
