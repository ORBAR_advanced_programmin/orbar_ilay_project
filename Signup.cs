﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
namespace TriviaClient
{
    public partial class Signup : Form
    {
        public  Socket soc;
        public Signup(Socket socket)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            soc = socket;
            errorLabel.Hide();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void signupbutton_Click(object sender, EventArgs e)
        {
            Helper h = new Helper(soc);
            string username = usernameText.Text;
            string password = passwordText.Text;
            string email = emailText.Text;
            string message = "203" + username.Length.ToString().PadLeft(2, '0') + username + password.Length.ToString().PadLeft(2, '0') + password + email.Length.ToString().PadLeft(2, '0') + email;
            h.send(message);
            string answer = h.recieve();
            if (answer == "1040")
            {
                this.Close();
            }
            errorLabel.Show();
            if (answer == "1041")
                errorLabel.Text = ("Password is illegal");
            else if (answer == "1042")
                errorLabel.Text = ("Username is already exists");
            else if (answer == "1043")
                errorLabel.Text = ("Username is illeagal");
            else
                errorLabel.Text = ("Other");
        }

        private void signoutbutton_Click(object sender, EventArgs e)
        {
            Helper h = new Helper(soc);
            h.send("201");
        }


    }
}
