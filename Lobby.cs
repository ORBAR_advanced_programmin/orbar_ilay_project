﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
namespace TriviaClient
{
    public partial class Lobby : Form
    {
        public  Socket soc;
        public  string roomName;
        public  string playersNumber;
        public  string questionNumber;
        public  string questionTime;
        public bool quit = true;
        public Lobby(Socket socket,string roomN,string playersN,string questionN,string questionT,string user,bool isAdmin)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            soc = socket;
            roomName = roomN;
            playersNumber = playersN;
            questionNumber = questionN;
            questionTime = questionT;
            roomNameLabel.Text = roomN;
            headLineLabel.Text = "You are connected to room " + roomN; 
            if(playersN == "")
            {
                detailsLabel.Text = "number_of_questions:  " + questionNumber + "   time_per_question:  " + questionTime;
            }
            else
            {
                detailsLabel.Text = "max_players_number:  " + playersNumber + "   number_of_questions:  " + questionNumber + "   time_per_question:  " + questionTime;
            }
            if (isAdmin)
            {
                leaveButton.Hide();
            }
            else
            {
                closeroombutton.Hide();
                startgamebutton.Hide();
            }
            listBox1.Items.Add(user);
            Thread t = new Thread(listen);
            t.Start();
        }
        private void listen()
        {
            Helper h = new Helper(soc);
            string messageCode = "";
            string answer = "";
            while (true && quit)
            {

                answer = h.recieve();
                messageCode = h.get_part(ref answer, 3);
                //close room
                if (messageCode == "116")
                {
                    break;
                }
                //start game
                else if(messageCode == "118")
                {
                    break;
                }
                //end game
                else if(messageCode == "121")
                {
                    break;
                }
                //leave room
                else if(messageCode == "112")
                {

                    break;
                }
                //add users to room
                else if(messageCode == "108")
                {
                    string numberOfUsers = h.get_part(ref answer, 1);
                    //MessageBox.Show(messageCode);
                    string length = "";
                    string username = "";
                    listBox1.Items.Clear();
                    while (answer != "")
                    {
                        length = h.get_part(ref answer, 2);
                        username = h.get_part(ref answer, Int32.Parse(length));
                        listBox1.Items.Add(username);
                    }
                }          
            }
            quit = true;
            if (messageCode == "118")
                handleStartGame(answer);
            else if (messageCode == "121")
                handleEndGame(answer);
        }
        private void closeroombutton_Click(object sender, EventArgs e)
        {
            Helper h = new Helper(soc);
            h.send("215");
            this.Close();
        }

        private void startgamebutton_Click(object sender, EventArgs e)
        {
            Helper h = new Helper(soc);
            h.send("217");
            this.Hide();

        }
        public void handleStartGame(string answer)
        {
            Helper h = new Helper(soc);
            string questionLength = h.get_part(ref answer, 3);
            string question = h.get_part(ref answer, Int32.Parse(questionLength));
            string answer1Length = h.get_part(ref answer, 3);
            string answer1 = h.get_part(ref answer, Int32.Parse(answer1Length));

            string answer2Length = h.get_part(ref answer, 3);
            string answer2 = h.get_part(ref answer, Int32.Parse(answer2Length));

            string answer3Length = h.get_part(ref answer, 3);
            string answer3 = h.get_part(ref answer, Int32.Parse(answer3Length));

            string answer4Length = h.get_part(ref answer, 3);
            string answer4 = h.get_part(ref answer, Int32.Parse(answer4Length));

            Form g = new Game(soc, roomName, question, answer1, answer2, answer3, answer4,questionNumber, questionTime);
            DialogResult r = g.ShowDialog();
            if (r == DialogResult.Abort)
            {
                quit = false;
                this.Close();
            }
            Thread t = new Thread(listen);
            t.Start();
        }
        public void handleEndGame(string answer)
        {
            Helper h = new Helper(soc);
            Game.score = 0;
            Game.questionId = 0;
            string usersNumber = h.get_part(ref answer, 1);
            string length;
            string username;
            string score;
            string results = "Scores:\n";
            while(answer!="")
            {
                length = h.get_part(ref answer, 2);
                username = h.get_part(ref answer, Int32.Parse(length));
                score = h.get_part(ref answer, 2);
                results += username + ": " + Int32.Parse(score).ToString() + "\n";
            }
            MessageBox.Show(results);
            quit = false;
            this.Close();
        }

        private void leaveButton_Click(object sender, EventArgs e)
        {
            Helper h = new Helper(soc);
            h.send("211");
            quit = false;
            this.Close();
        }
    }
}
