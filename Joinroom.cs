﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
namespace TriviaClient
{
    public partial class Joinroom : Form
    {
        public Socket soc;
        public string PubroomName;
        public string PubroomNumber;
        public string usernameIn;
        public Joinroom(Socket socket,string user)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            usernameIn = user;
            soc = socket;
            errorLabel.Text = "";
            usersLabel.Hide();
            listBox2.Hide();
            listen();
        }
        private void listen()
        {
            Helper h = new Helper(soc);
            string answer = h.recieve();
            string msgCode = h.get_part(ref answer, 3);
            string numberOfRooms = h.get_part(ref answer, 4);
            string roomId;
            string length;
            string roomName;
            while(answer!="")
            {
                roomId = Int32.Parse(h.get_part(ref answer, 4)).ToString();
                length = h.get_part(ref answer, 2);
                roomName = h.get_part(ref answer, Int32.Parse(length));
                listBox1.Items.Add(roomId + "." + roomName);
            }
        }
        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            Helper h = new Helper(soc);
            h.send("205");
            listen();
        }

        private void joinLabel_Click(object sender, EventArgs e)
        {
            try
            {
                Helper h = new Helper(soc);
                h.send("209" + PubroomNumber.PadLeft(4, '0'));
                string answer = h.recieve();
                string msgCode = h.get_part(ref answer, 3);
                string code = h.get_part(ref answer, 1);
                if (code == "0")
                {
                    string questionNumber = Int32.Parse(h.get_part(ref answer, 2)).ToString();
                    string questionTime = Int32.Parse(h.get_part(ref answer, 2)).ToString();
                    this.Hide();
                    Form lb = new Lobby(soc, PubroomName, "", questionNumber, questionTime, usernameIn, false);
                    lb.ShowDialog();
                    this.Close();
                }
                else if (code == "1")
                {
                    errorLabel.Text = "room is full";
                }
                //code = 2
                else
                {
                    errorLabel.Text = "room not exist or other reason";
                }
            }
            catch (Exception)
            {

            }

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            usersLabel.Show();
            listBox2.Show();
            try
            {
                string currItem = listBox1.SelectedItem.ToString();
                //MessageBox.Show(currItem);
                PubroomNumber = currItem.Split('.')[0];
                //MessageBox.Show(roomNumber);
                PubroomName = currItem.Split('.')[1];
                //MessageBox.Show(roomName);
                Helper h = new Helper(soc);
                h.send("207" + PubroomNumber.PadLeft(4, '0'));
                string answer = h.recieve();
                string msgCode = h.get_part(ref answer, 3);
                string usersN = h.get_part(ref answer, 1);
                string length;
                string username;
                while (answer != "")
                {
                    length = h.get_part(ref answer, 2);
                    username = h.get_part(ref answer, Int32.Parse(length));
                    listBox2.Items.Add(username);
                }
            }
            //happens when clicking on an empty item
            catch(Exception)
            {

            }
            
        }
    }
}
