#pragma once
#include <condition_variable>
#include <mutex>
#include <map>
#include <queue>
#include <iostream>
#include <thread>
#include "Helper.h"
#include "RecievedMessage.h"
#include "User.h"
#include "Room.h"
#include "DataBase.h"
#include "Validator.h"
#include <sstream>
#include <iomanip>
using namespace std;
class TriviaServer
{
public:

	TriviaServer();
	~TriviaServer();
	void serve();
private:
	std::vector<std::string> split(std::string strToSplit, char delimeter);
	void bindAndListen();
	void acceptClient();
	void clientHandler(SOCKET client_socket);
	void safeDeleteUser(RecievedMessage* msg);
	User* handleSignIn(RecievedMessage* msg);
	bool handleSignUp(RecievedMessage* msg);
	void handleSignOut(RecievedMessage* msg);
	bool handleCreateRoom(RecievedMessage* msg);
	bool handleCloseRoom(RecievedMessage* msg);
	bool handleJoinRoom(RecievedMessage* msg);
	bool handleLeaveRoom(RecievedMessage* msg);
	void handleGetUsersInRoom(RecievedMessage* msg);
	void handleGetRooms(RecievedMessage* msg);
	void handleLeaveGame(RecievedMessage* msg);
	void handleStartGame(RecievedMessage* msg);
	void handlePlayerAnswer(RecievedMessage* msg);
	void addRecievedMessage(RecievedMessage* msg);
	void handleGetBestScores(RecievedMessage* msg);
	void handleGetPersonalStatus(RecievedMessage* msg);
	void handleRecievedMessages();
	RecievedMessage* buildRecieveMessage(SOCKET client_socket, int msgCode);
	User* getUserByName(string name);
	User* getUserBySocket(SOCKET client_socket);
	Room* getRoomByid(int id);
	SOCKET _socket;
	DataBase _db;
	mutex _mtxRecievedMessages;
	queue<RecievedMessage*> _queRcvMessages;
	std::condition_variable _msgQueueCondition;
	map<SOCKET, User*> _connectedUsers;
	map<int, Room*> _roomsList;
	int _roomSequence;
};

