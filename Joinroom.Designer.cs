﻿namespace TriviaClient
{
    partial class Joinroom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backButton = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.headLineLabel = new System.Windows.Forms.Label();
            this.refreshButton = new System.Windows.Forms.Button();
            this.joinLabel = new System.Windows.Forms.Button();
            this.errorLabel = new System.Windows.Forms.Label();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.usersLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.backButton.Location = new System.Drawing.Point(683, 3);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(105, 43);
            this.backButton.TabIndex = 0;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(249, 40);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(228, 104);
            this.listBox1.TabIndex = 1;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // headLineLabel
            // 
            this.headLineLabel.AutoSize = true;
            this.headLineLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.headLineLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.headLineLabel.Location = new System.Drawing.Point(307, 12);
            this.headLineLabel.Name = "headLineLabel";
            this.headLineLabel.Size = new System.Drawing.Size(119, 25);
            this.headLineLabel.TabIndex = 2;
            this.headLineLabel.Text = "Rooms List: ";
            // 
            // refreshButton
            // 
            this.refreshButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.refreshButton.Location = new System.Drawing.Point(312, 331);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(93, 40);
            this.refreshButton.TabIndex = 3;
            this.refreshButton.Text = "refresh";
            this.refreshButton.UseVisualStyleBackColor = true;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // joinLabel
            // 
            this.joinLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.joinLabel.Location = new System.Drawing.Point(303, 385);
            this.joinLabel.Name = "joinLabel";
            this.joinLabel.Size = new System.Drawing.Size(123, 53);
            this.joinLabel.TabIndex = 4;
            this.joinLabel.Text = "Join";
            this.joinLabel.UseVisualStyleBackColor = true;
            this.joinLabel.Click += new System.EventHandler(this.joinLabel_Click);
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.errorLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.errorLabel.Location = new System.Drawing.Point(334, 147);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(52, 25);
            this.errorLabel.TabIndex = 5;
            this.errorLabel.Text = "error";
            // 
            // listBox2
            // 
            this.listBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 20;
            this.listBox2.Location = new System.Drawing.Point(274, 221);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(175, 64);
            this.listBox2.TabIndex = 6;
            // 
            // usersLabel
            // 
            this.usersLabel.AutoSize = true;
            this.usersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.usersLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.usersLabel.Location = new System.Drawing.Point(315, 193);
            this.usersLabel.Name = "usersLabel";
            this.usersLabel.Size = new System.Drawing.Size(74, 25);
            this.usersLabel.TabIndex = 7;
            this.usersLabel.Text = "Users: ";
            // 
            // Joinroom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.usersLabel);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.joinLabel);
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.headLineLabel);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.backButton);
            this.Name = "Joinroom";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label headLineLabel;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.Button joinLabel;
        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label usersLabel;
    }
}