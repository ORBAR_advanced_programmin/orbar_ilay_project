#pragma once
#include "DataBase.h"
#include <string>
#include <map>
#include <vector>
#include "User.h"
#include "Question.h"
#include "sqlite3.h"
#include "Helper.h"
#include <sstream>
#include <iomanip>

using namespace std;

class User;

class Game
{
public:

	Game(int id,DataBase & db,int questionNo, const vector<User*>& players);
	~Game();
	void sendQuestionToAllUsers();
	void handleFinishGame();
	bool handleNextTurn();
	void sendFirstQuestion();
	bool handleAnswerFromUser(User* user,int answerNo,int time);
	bool leaveGame(User* user);
	int getId();
private:

	vector<Question*> _questions;
	vector<User*> _players;
	int _question_no;
	int _currQuestionIndex;
	DataBase & _db;
	map<string, int> _results;
	int _currentTurnAnswers;
	int _id;
};
