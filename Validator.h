#pragma once
#include <iostream>
#include <string>
using namespace std;
class Validator
{
public:
	Validator();
	~Validator();
	static bool isPasswordValid(string password);
	static bool isUsernameValid(string username);
};

