﻿namespace TriviaClient
{
    partial class BestScores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backButton = new System.Windows.Forms.Button();
            this.bestScoresLabel = new System.Windows.Forms.Label();
            this.bestScoresBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.backButton.Location = new System.Drawing.Point(681, 25);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(92, 38);
            this.backButton.TabIndex = 0;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // bestScoresLabel
            // 
            this.bestScoresLabel.AutoSize = true;
            this.bestScoresLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.bestScoresLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.bestScoresLabel.Location = new System.Drawing.Point(324, 69);
            this.bestScoresLabel.Name = "bestScoresLabel";
            this.bestScoresLabel.Size = new System.Drawing.Size(129, 25);
            this.bestScoresLabel.TabIndex = 1;
            this.bestScoresLabel.Text = "Best Scores: ";
            // 
            // bestScoresBox
            // 
            this.bestScoresBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.bestScoresBox.Location = new System.Drawing.Point(210, 125);
            this.bestScoresBox.Name = "bestScoresBox";
            this.bestScoresBox.Size = new System.Drawing.Size(345, 247);
            this.bestScoresBox.TabIndex = 2;
            this.bestScoresBox.Text = "";
            // 
            // BestScores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.bestScoresBox);
            this.Controls.Add(this.bestScoresLabel);
            this.Controls.Add(this.backButton);
            this.Name = "BestScores";
            this.Text = "BestScores";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Label bestScoresLabel;
        private System.Windows.Forms.RichTextBox bestScoresBox;
    }
}