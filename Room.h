#pragma once
#include <WinSock2.h>
#include <vector>
#include <string>
#include "Helper.h"
#include "User.h"
#include <sstream>
#include <iomanip>
class User;
class Helper;

using namespace std;

class Room
{
public:
	Room(int id,User* admin,string name,int maxUsers,int questionsNo,int questionTime);
	~Room();
	string getUsersAsString();
	string getUsersListMessage();
	int getId();
	vector<User*> getUsers();
	string getName();
	int getQuestionNo();
	int getQuestionTime();
	void sendMessage(User* ExcludeUser, string msg);
	void sendMessage(string msg);
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User * user);
	vector<User*> _users;
private:
	
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNum;
	string _name;
	int _id;
};

