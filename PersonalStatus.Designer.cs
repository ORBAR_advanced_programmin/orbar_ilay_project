﻿namespace TriviaClient
{
    partial class PersonalStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.headLineLabel = new System.Windows.Forms.Label();
            this.performanceBox = new System.Windows.Forms.RichTextBox();
            this.backButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // headLineLabel
            // 
            this.headLineLabel.AutoSize = true;
            this.headLineLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.headLineLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.headLineLabel.Location = new System.Drawing.Point(304, 45);
            this.headLineLabel.Name = "headLineLabel";
            this.headLineLabel.Size = new System.Drawing.Size(166, 25);
            this.headLineLabel.TabIndex = 1;
            this.headLineLabel.Text = "My Performance: ";
            // 
            // performanceBox
            // 
            this.performanceBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.performanceBox.Location = new System.Drawing.Point(202, 106);
            this.performanceBox.Name = "performanceBox";
            this.performanceBox.Size = new System.Drawing.Size(373, 289);
            this.performanceBox.TabIndex = 2;
            this.performanceBox.Text = "";
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.backButton.Location = new System.Drawing.Point(677, 12);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(97, 35);
            this.backButton.TabIndex = 3;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // PersonalStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.performanceBox);
            this.Controls.Add(this.headLineLabel);
            this.Name = "PersonalStatus";
            this.Text = "PersonalStatus";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label headLineLabel;
        private System.Windows.Forms.RichTextBox performanceBox;
        private System.Windows.Forms.Button backButton;
    }
}