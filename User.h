#pragma once

#include <string>
#include <WinSock2.h>
//#include "Room.h"
#include "Game.h"

class Room;
class Game;

using namespace std;

class User
{
	public:
		User(SOCKET skt,string username);
		~User();
		void send(string toSend);
		string getUserName();
		SOCKET getSocket();
		Room* getRoom();
		Game* getGame();
		void setGame(Game* gm);
		void setRoom(Room* r);
		void clearRoom();
		bool createRoom(int roomId,string roomName,int maxUsers,int questionsNo,int questionsTime);
		bool joinRoom(Room* newRoom);
		void leaveRoom();
		int closeRoom();
		bool leaveGame();
	private:
		string _username;
		Room*  _currRoom;
		Game* _currGame;
		SOCKET _sock;


};

