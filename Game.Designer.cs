﻿namespace TriviaClient
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.roomNameLabel = new System.Windows.Forms.Label();
            this.questionNumberLabel = new System.Windows.Forms.Label();
            this.questionLabel = new System.Windows.Forms.Label();
            this.answer1button = new System.Windows.Forms.Button();
            this.answer2Button = new System.Windows.Forms.Button();
            this.answer3Button = new System.Windows.Forms.Button();
            this.answer4Button = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.timeValueLabel = new System.Windows.Forms.Label();
            this.scoreLabel = new System.Windows.Forms.Label();
            this.scoreValueLabel = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // roomNameLabel
            // 
            this.roomNameLabel.AutoSize = true;
            this.roomNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.roomNameLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.roomNameLabel.Location = new System.Drawing.Point(321, 20);
            this.roomNameLabel.Name = "roomNameLabel";
            this.roomNameLabel.Size = new System.Drawing.Size(116, 25);
            this.roomNameLabel.TabIndex = 0;
            this.roomNameLabel.Text = "room_name";
            // 
            // questionNumberLabel
            // 
            this.questionNumberLabel.AutoSize = true;
            this.questionNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.questionNumberLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.questionNumberLabel.Location = new System.Drawing.Point(298, 59);
            this.questionNumberLabel.Name = "questionNumberLabel";
            this.questionNumberLabel.Size = new System.Drawing.Size(162, 25);
            this.questionNumberLabel.TabIndex = 1;
            this.questionNumberLabel.Text = "Question number";
            // 
            // questionLabel
            // 
            this.questionLabel.AutoSize = true;
            this.questionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.questionLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.questionLabel.Location = new System.Drawing.Point(94, 84);
            this.questionLabel.Name = "questionLabel";
            this.questionLabel.Size = new System.Drawing.Size(86, 25);
            this.questionLabel.TabIndex = 2;
            this.questionLabel.Text = "question";
            // 
            // answer1button
            // 
            this.answer1button.BackColor = System.Drawing.SystemColors.Control;
            this.answer1button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.answer1button.Location = new System.Drawing.Point(157, 121);
            this.answer1button.Name = "answer1button";
            this.answer1button.Size = new System.Drawing.Size(462, 41);
            this.answer1button.TabIndex = 3;
            this.answer1button.Text = "answer1";
            this.answer1button.UseVisualStyleBackColor = false;
            this.answer1button.Click += new System.EventHandler(this.answer1button_Click);
            // 
            // answer2Button
            // 
            this.answer2Button.BackColor = System.Drawing.SystemColors.Control;
            this.answer2Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.answer2Button.Location = new System.Drawing.Point(157, 177);
            this.answer2Button.Name = "answer2Button";
            this.answer2Button.Size = new System.Drawing.Size(462, 38);
            this.answer2Button.TabIndex = 4;
            this.answer2Button.Text = "answer2";
            this.answer2Button.UseVisualStyleBackColor = false;
            this.answer2Button.Click += new System.EventHandler(this.answer2Button_Click);
            // 
            // answer3Button
            // 
            this.answer3Button.BackColor = System.Drawing.SystemColors.Control;
            this.answer3Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.answer3Button.Location = new System.Drawing.Point(157, 232);
            this.answer3Button.Name = "answer3Button";
            this.answer3Button.Size = new System.Drawing.Size(462, 38);
            this.answer3Button.TabIndex = 5;
            this.answer3Button.Text = "answer3";
            this.answer3Button.UseVisualStyleBackColor = false;
            this.answer3Button.Click += new System.EventHandler(this.answer3Button_Click);
            // 
            // answer4Button
            // 
            this.answer4Button.BackColor = System.Drawing.SystemColors.Control;
            this.answer4Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.answer4Button.Location = new System.Drawing.Point(157, 288);
            this.answer4Button.Name = "answer4Button";
            this.answer4Button.Size = new System.Drawing.Size(462, 38);
            this.answer4Button.TabIndex = 6;
            this.answer4Button.Text = "answer4";
            this.answer4Button.UseVisualStyleBackColor = false;
            this.answer4Button.Click += new System.EventHandler(this.answer4Button_Click);
            // 
            // exitButton
            // 
            this.exitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.exitButton.Location = new System.Drawing.Point(701, 13);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(87, 32);
            this.exitButton.TabIndex = 7;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // timeValueLabel
            // 
            this.timeValueLabel.AutoSize = true;
            this.timeValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.timeValueLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.timeValueLabel.Location = new System.Drawing.Point(368, 338);
            this.timeValueLabel.Name = "timeValueLabel";
            this.timeValueLabel.Size = new System.Drawing.Size(23, 25);
            this.timeValueLabel.TabIndex = 8;
            this.timeValueLabel.Text = "0";
            // 
            // scoreLabel
            // 
            this.scoreLabel.AutoSize = true;
            this.scoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.scoreLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.scoreLabel.Location = new System.Drawing.Point(285, 382);
            this.scoreLabel.Name = "scoreLabel";
            this.scoreLabel.Size = new System.Drawing.Size(75, 25);
            this.scoreLabel.TabIndex = 9;
            this.scoreLabel.Text = "Score: ";
            // 
            // scoreValueLabel
            // 
            this.scoreValueLabel.AutoSize = true;
            this.scoreValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.scoreValueLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.scoreValueLabel.Location = new System.Drawing.Point(366, 382);
            this.scoreValueLabel.Name = "scoreValueLabel";
            this.scoreValueLabel.Size = new System.Drawing.Size(23, 25);
            this.scoreValueLabel.TabIndex = 10;
            this.scoreValueLabel.Text = "0";
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.timeLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.timeLabel.Location = new System.Drawing.Point(285, 338);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(67, 25);
            this.timeLabel.TabIndex = 11;
            this.timeLabel.Text = "Time: ";
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuText;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.scoreValueLabel);
            this.Controls.Add(this.scoreLabel);
            this.Controls.Add(this.timeValueLabel);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.answer4Button);
            this.Controls.Add(this.answer3Button);
            this.Controls.Add(this.answer2Button);
            this.Controls.Add(this.answer1button);
            this.Controls.Add(this.questionLabel);
            this.Controls.Add(this.questionNumberLabel);
            this.Controls.Add(this.roomNameLabel);
            this.Name = "Game";
            this.Text = "Game";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label roomNameLabel;
        private System.Windows.Forms.Label questionNumberLabel;
        private System.Windows.Forms.Label questionLabel;
        private System.Windows.Forms.Button answer1button;
        private System.Windows.Forms.Button answer2Button;
        private System.Windows.Forms.Button answer3Button;
        private System.Windows.Forms.Button answer4Button;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label timeValueLabel;
        private System.Windows.Forms.Label scoreLabel;
        private System.Windows.Forms.Label scoreValueLabel;
        private System.Windows.Forms.Label timeLabel;
    }
}