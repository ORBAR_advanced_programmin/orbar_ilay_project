﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
namespace TriviaClient
{
    public partial class PersonalStatus : Form
    {
        public Socket soc;
        public PersonalStatus(Socket socket,string gameN,string correctA,string wrongA,string number)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            soc = socket;
            performanceBox.ReadOnly = true;
            performanceBox.Text = "number of games: " + gameN + "\ncorrent answers: " + correctA + "\nwrong answers: " + wrongA + "\naverage time for answer: " + number;

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
