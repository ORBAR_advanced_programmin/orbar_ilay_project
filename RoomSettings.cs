﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
namespace TriviaClient
{
    public partial class RoomSettings : Form
    {
        public  Socket soc;
        public  string usernameIn;
        public RoomSettings(Socket socket,string user)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            soc = socket;
            usernameIn = user;
            errorLabel.Text = "";
        }

        private void Sendbutton_Click(object sender, EventArgs e)
        {
            try
            {
                string roomN = roomName.Text;
                string playersNumber = playersN.Text;
                string questionsNumber = questionsN.Text;
                string questionsTime = questionsT.Text;
                if (Int32.Parse(playersNumber) >= 10)
                {
                    errorLabel.Text = "Wrong players number";
                }
                else if (Int32.Parse(questionsNumber) >= 100)
                {
                    errorLabel.Text = "Wrong question number";
                }
                else if (Int32.Parse(questionsTime) >= 100)
                {
                    errorLabel.Text = "Wrong question time";
                }
                else
                {
                    errorLabel.Text = "";
                    string message = "213";
                    message += roomN.Length.ToString().PadLeft(2, '0') + roomN + playersNumber + questionsNumber.PadLeft(2, '0') + questionsTime.PadLeft(2, '0');
                    Helper h = new Helper(soc);
                    h.send(message);
                    string answer = h.recieve();
                    if (answer == "1140")
                    {
                        this.Hide();
                        Form lb = new Lobby(soc, roomN, playersNumber, questionsNumber, questionsTime, usernameIn, true);
                        lb.ShowDialog();
                        this.Close();
                    }
                }
            }         
            catch(Exception)
            {

            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }
    }
}
