#include "RecievedMessage.h"



RecievedMessage::RecievedMessage(SOCKET sock, int messageCode)
{
	_sock = sock;
	_messageCode = messageCode;
}

RecievedMessage::RecievedMessage(SOCKET sock, int messageCode, vector<string> values)
{
	_sock = sock;
	_values = values;
	_messageCode = messageCode;
}

RecievedMessage::~RecievedMessage()
{
}
SOCKET RecievedMessage::getSock()
{
	return _sock;
}
int RecievedMessage::getMessageCode()
{
	return _messageCode;
}
User* RecievedMessage::getUser()
{
	return _user;
}
void RecievedMessage::setUser(User* user)
{
	_user = user;
}
vector<string>& RecievedMessage::getValues()
{
	return _values;
}
